import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { ValidationPipe } from '@nestjs/common'

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true })

  app.useGlobalPipes(
    new ValidationPipe({
      disableErrorMessages:
        process.env.ENVIRONMENT === 'production' ? true : false,
      transform: true
    })
  )

  await app.listen(process.env.port || 3002)
}

bootstrap()
