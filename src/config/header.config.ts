const defaultHeader: any = {
  'Content-Type': 'application/json'
}

const cartHeader: any = {
  'Content-Type': 'application/json',
  'Accept-Encoding': 'gzip',
  'X-Application': 'payment-wrapper',
  'Modify-Items': 1
}

const cartResetHeader: any = {
  'Content-Type': 'application/json',
  'Accept-Encoding': 'gzip',
  'X-Application': 'payment-wrapper',
  'Modify-Items': 1,
  'Reset-Shipping': 1
}

const emailHeader = (emailAndPhone: string, companyCode: string): any => ({
  'Content-Type': 'application/json',
  'Accept-Encoding': 'gzip',
  'X-Application': 'payment-wrapper',
  'rr-email-and-phone': emailAndPhone,
  'rr-company-code': companyCode
})

const sosmedHeader = (
  sosmed: string,
  code: string,
  fingerprint: string
): any => ({
  'Content-Type': 'application/json',
  'Accept-Encoding': 'gzip',
  'X-Application': 'payment-wrapper',
  'rr-type': sosmed,
  'rr-requester': 'payment',
  'rr-code': code || '',
  'rr-fingerprint': fingerprint || ''
})

const tpsHeader: any = {
  'Content-Type': 'application/json',
  'Accept-Encoding': 'gzip',
  'X-Application': 'payment-wrapper',
  'X-Rate-Limited': 10
}

const customerHeader: any = (customerId: string) => ({
  'Content-Type': 'application/json',
  'Accept-Encoding': 'gzip',
  'X-Application': 'payment-wrapper',
  'rr-customer-id': parseInt(customerId)
})

const cartNoRefreshHeader = {
  'Content-Type': 'application/json',
  'Accept-Encoding': 'gzip',
  'X-Application': 'payment-wrapper',
  'No-Refresh': 1
}

export {
  defaultHeader,
  cartHeader,
  cartResetHeader,
  emailHeader,
  sosmedHeader,
  tpsHeader,
  customerHeader,
  cartNoRefreshHeader
}
