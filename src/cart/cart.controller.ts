import {
  Controller,
  Get,
  Post,
  Param,
  Body,
  HttpCode,
  Headers
} from '@nestjs/common'
import { CartService } from './cart.service'
import {
  CartUpdateData,
  CartDeleteItemData,
  CartRecreateData,
  CartCarrierAssignationData
} from './cart.dto'

@Controller('cart')
export class CartController {
  constructor(private cartService: CartService) {}

  @Get(':cartId')
  getCart(@Param('cartId') cartId: string): Promise<any> | object {
    return this.cartService.getCart(cartId)
  }

  @Get('validate/:cartId')
  validateCart(@Param('cartId') cartId: string): Promise<any> | object {
    return this.cartService.validateCart(cartId)
  }

  @Post('update')
  @HttpCode(200)
  updateCart(
    @Headers() header: any,
    @Body() data: CartUpdateData
  ): Promise<any> | object {
    return this.cartService.updateCart(header, data)
  }

  @Post('delete')
  @HttpCode(200)
  deleteCartItem(
    @Headers() header: any,
    @Body() data: CartDeleteItemData
  ): Promise<any> | object {
    return this.cartService.deleteCartItem(header, data)
  }

  @Get('reset/:cartId')
  getResetCart(@Param('cartId') cartId: string): Promise<any> | object {
    return this.cartService.getResetCart(cartId)
  }

  @Get('cashback/:cartId')
  cashbackCart(@Param('cartId') cartId: string): Promise<any> | object {
    return this.cartService.cashbackCart(cartId)
  }

  @Post('assignation/carrier-validation')
  @HttpCode(200)
  checkCarrierAssignationValidation(
    @Headers() header: any,
    @Body() data: CartCarrierAssignationData
  ): Promise<any> | object {
    return this.cartService.checkCarrierAssignationValidation(header, data)
  }

  @Post('recreate')
  @HttpCode(200)
  recreateCart(
    @Headers() header: any,
    @Body() data: CartRecreateData
  ): Promise<any> | object {
    return this.cartService.recreate(header, data)
  }
}
