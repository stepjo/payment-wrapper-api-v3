import { Injectable, HttpException } from '@nestjs/common'
import { HttpService } from '@nestjs/axios'
import { map, catchError, firstValueFrom } from 'rxjs'
import {
  defaultHeader,
  cartHeader,
  cartResetHeader
} from 'src/config/header.config'
import {
  ResponseErrorMessage,
  CatchErrorMessage
} from 'src/utils/error/ErrorMessage.utils'
import { CheckErrorResponse } from 'src/utils/error/ErrorResponse.utils'
import * as moment from 'moment'
import {
  CartCarrierAssignationData,
  CartDeleteItemData,
  CartRecreateData,
  CartUpdateData
} from './cart.dto'
import { JwtService } from '@nestjs/jwt'
import { isEmpty } from 'validate.js'

const excludeMinicartUserAgent = ['ace-ios', 'ace-android']

const getCartDetail = (response: any): object => {
  const result: any = response?.data?.data
  let flagAllPickup: any = 1
  let isThereAnyInformaItem: any = 0

  result?.items?.map((data: any) => {
    if (
      data?.shipping?.delivery_method === 'delivery' ||
      data?.shipping?.delivery_method === 'store_fulfillment' ||
      data?.shipping?.delivery_method === 'ownfleet'
    ) {
      flagAllPickup = 0
    }

    const date: any = data?.shipping?.preorder_date

    if (date) {
      const [month, day, year] = date.split('/')
      data.shipping.preorder_date = `${day || ''}${month ? `/${month}` : ''}${
        year ? `/${year}` : ''
      }`
    }
  })

  if (flagAllPickup === 0) {
    result.all_pickup = false
  } else {
    result.all_pickup = true
  }

  result.total_items = result?.items?.reduce(
    (prev: any, next: any) => prev + next?.details?.length,
    0
  )

  if (!isEmpty(result?.items)) {
    result?.items?.map((item: any) => {
      item?.details?.map((detail: any) => {
        if (
          detail?.supplier_alias === 'HCI' ||
          detail?.supplier_alias_original === 'HCI'
        ) {
          isThereAnyInformaItem++
        }
      })
    })
  }

  if (isThereAnyInformaItem > 0) {
    result.informa_item_present = true
  } else {
    result.informa_item_present = false
  }

  return {
    errors: response.data.errors,
    message: 'success',
    data: result
  }
}

const fetchCartReset = (response: any): object => {
  const result: any = response?.data?.data
  let flagAllPickup = 1

  result?.items?.map((data: any) => {
    if (
      data?.shipping?.delivery_method === 'delivery' ||
      data?.shipping?.delivery_method === 'store_fulfillment' ||
      data?.shipping?.delivery_method === 'ownfleet'
    ) {
      flagAllPickup = 0
    }
  })

  // flag all pickup
  if (flagAllPickup === 0) {
    result.all_pickup = false
  } else {
    result.all_pickup = true
  }

  result.total_items = result?.items?.reduce(
    (prev: any, next: any) => prev + next.details.length,
    0
  )

  return {
    errors: response?.data?.errors,
    message: 'success',
    data: result
  }
}

const GetVaExpire = (date: any, maximum: any): boolean => {
  const orderTime: any = moment(date)
  const currentTime: any = moment(new Date())
  const hoursDiff: any = currentTime.diff(orderTime, 'hours')

  return hoursDiff < maximum
}

const GetChangePaymentAvailable = (cart: any): void => {
  const payment: any = cart?.payment

  if (cart?.status === 'processing') {
    if (payment?.status === 'pending' && payment?.type === 'bank_transfer') {
      payment.has_changed_payment = parseInt(payment.has_changed_payment)

      if (
        payment.has_changed_payment !== -1 &&
        payment.has_changed_payment < 2
      ) {
        const available = GetVaExpire(payment.created_at, 4)

        if (!available) {
          payment.has_changed_payment = -1
        }
      } else {
        payment.has_changed_payment = -1
      }

      if (payment.can_cancel === 10) {
        cart.can_cancel_order = true
      } else {
        cart.can_cancel_order = false
      }
    } else {
      payment.has_changed_payment = -1
      cart.can_cancel_order = false
    }

    // Repeat order
    if (payment?.status === 'canceled') {
      cart.can_repeat_order = true
    } else {
      cart.can_repeat_order = false
    }
  }
}

@Injectable({})
export class CartService {
  constructor(
    private httpService: HttpService,
    private jwtService: JwtService
  ) {}

  getCart(cartId: string): Promise<any> | object {
    return this.httpService
      .get(`${process.env.CART_API_URL}cart/${cartId}`, { headers: cartHeader })
      .pipe(
        map(async (response: any): Promise<any> => {
          const result: any = await getCartDetail(response)

          if (result?.data?.customer) {
            result.data.customer.cid = result.data.customer.customer_id
          }

          GetChangePaymentAvailable(result?.data)

          return result
        }),
        catchError((e: any): Promise<any> => {
          const result: any = getCartDetail(e?.response)

          if (result?.data?.customer) {
            result.data.customer.cid = result.data.customer.customer_id
          }

          GetChangePaymentAvailable(result?.data)

          return result
        })
      )
  }

  validateCart(cartId: string): Promise<any> | object {
    return this.httpService
      .get(`${process.env.CART_API_URL}cart/validate/${cartId}`, {
        headers: defaultHeader
      })
      .pipe(
        map((response: any): Promise<any> | object => {
          return {
            errors: {},
            message: 'success',
            data: response?.data?.data
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  async updateCart(header: any, data: CartUpdateData): Promise<any> {
    try {
      const { data: payload }: any = data

      if (!payload) {
        const errorMessage: any = ResponseErrorMessage({}, '', false, 'error')

        throw new HttpException(errorMessage.payload, errorMessage.status)
      }

      payload.remote_ip =
        header['x-forwarded-for'] || header?.connection?.remoteAddress
      payload.company_code = process.env.COMPANY_CODE

      if (payload?.customer?.customer_email) {
        payload.customer.customer_email = data.customer.customer_email
          .trim()
          .toLowerCase()
      }

      // if (payload?.customer?.customer_id) {
      //   payload.customer.customer_id = await this.jwtService.signAsync(
      //     payload?.customer?.customer_id.toString()
      //   )
      // }

      const itemsForMinicart: any = {}

      // validate qty of the items, if exist
      if (!isEmpty(payload?.items)) {
        payload.items.forEach((val: any, idx: number) => {
          if (val.hasOwnProperty('qty_ordered')) {
            if (!isEmpty(val?.qty_ordered) && parseInt(val?.qty_ordered) < 1) {
              payload.items[idx].qty_ordered = 1
            }

            itemsForMinicart[val?.sku] = val
          }
        })
      }

      const updateCartResponse: any = await firstValueFrom(
        this.httpService
          .put(
            `${process.env.CART_API_URL}cart/update`,
            { data: payload },
            { headers: defaultHeader }
          )
          .pipe(
            map((response: any): Promise<any> | object => {
              return response
            })
          )
      )

      if (
        payload?.is_update_address &&
        CheckErrorResponse(updateCartResponse?.data?.errors)
      ) {
        throw updateCartResponse?.data
      }

      const userAgent: string = header['user-agent'] + ''

      if (!excludeMinicartUserAgent.includes(userAgent.toLowerCase())) {
        const getCartResponse: any = await firstValueFrom(
          this.httpService
            .get(`${process.env.CART_API_URL}cart/${payload?.cart_id}`, {
              headers: defaultHeader
            })
            .pipe(
              map((response: any): Promise<any> | object => {
                return response
              })
            )
        )

        if (CheckErrorResponse(getCartResponse?.data?.errors)) {
          throw getCartResponse?.data
        }

        const cartData: any = getCartResponse?.data?.data
        const cartItems: Array<any> = cartData?.items || []

        if (cartItems?.length > 0) {
          const unifiedData: object = cartItems?.map((item) => {
            const data: object = {
              sku: item?.sku,
              qty_ordered: item?.qty_ordered,
              shipping: item?.shipping && {
                delivery_method: item?.shipping?.delivery_method,
                store_code: item?.shipping?.store_code
              },
              is_checked: true
            }

            return itemsForMinicart[item.sku]
              ? { ...data, ...itemsForMinicart[item.sku] }
              : data
          })

          const finalData: object = {
            ...data,
            items: unifiedData,
            minicart_id: payload?.minicart_id,
            customer: cartData?.customer
          }

          // Update mini cart and ignore response just like previous implementation
          await firstValueFrom(
            this.httpService.put(
              `${process.env.CART_API_URL}cart/minicart/update`,
              { data: finalData },
              { headers: defaultHeader }
            )
          )
        }
      }

      //  TEMPORARY HACK //
      // updateCartResponse.data.data.cart_id =
      //   '4a15e315a2f77650b79f643cdf973e7960b1003d'

      return {
        errors: updateCartResponse?.data?.errors,
        message: 'success',
        data: updateCartResponse?.data?.data
      }
    } catch (e) {
      const errorMessage: any = CatchErrorMessage(e, '', false)

      throw new HttpException(errorMessage.payload, errorMessage.status)
    }
  }

  getResetCart(cartId: string): Promise<any> | object {
    return this.httpService
      .get(`${process.env.CART_API_URL}cart/${cartId}`, {
        headers: cartResetHeader
      })
      .pipe(
        map((response: any): Promise<any> => {
          const result: any = fetchCartReset(response)
          result.data.customer.cid = result.data.customer.customer_id

          return result
        }),
        catchError((e: any): any => {
          const result: any = fetchCartReset(e?.response)

          if (result?.data?.customer) {
            result.data.customer.cid = result.data.customer.customer_id
          }

          return result
        })
      )
  }

  async deleteCartItem(header: any, data: CartDeleteItemData): Promise<any> {
    const { data: payload }: any = data
    const userAgent: string = header['user-agent'] + ''

    if (!excludeMinicartUserAgent.includes(userAgent?.toLowerCase())) {
      const minicartData: any = {
        ...payload,
        minicart_id: payload?.minicart_id
      }

      await firstValueFrom(
        this.httpService.put(
          `${process.env.CART_API_URL}cart/minicart/remove_item`,
          minicartData,
          { headers: defaultHeader }
        )
      )
    }

    const cartDeleteResponse: any = await firstValueFrom(
      this.httpService
        .put(`${process.env.CART_API_URL}cart/remove_item`, payload, {
          headers: defaultHeader
        })
        .pipe(
          map((response: any): Promise<any> | object => {
            return response
          }),
          catchError((e: any): Promise<any> => {
            const errorMessage: any = CatchErrorMessage(e, '', false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          })
        )
    )

    if (cartDeleteResponse?.status === 200) {
      const updateResponse: any = await firstValueFrom(
        this.httpService
          .put(
            `${process.env.CART_API_URL}cart/update`,
            {
              cart_id: payload?.cart_id,
              customer: payload?.customer
            },
            { headers: defaultHeader }
          )
          .pipe(
            map((response: any): Promise<any> | object => {
              return response
            }),
            catchError((e: any): Promise<any> => {
              const errorMessage: any = CatchErrorMessage(e, '', false)

              throw new HttpException(errorMessage.payload, errorMessage.status)
            })
          )
      )

      return {
        errors: updateResponse?.data?.errors,
        message: 'success',
        data: updateResponse?.data?.data
      }
    }
  }

  cashbackCart(cartId: string): Promise<any> | object {
    return this.httpService
      .get(`${process.env.CART_API_URL}marketing/cashback/${cartId}`, {
        headers: defaultHeader
      })
      .pipe(
        map((response: any): Promise<any> | object => {
          return {
            error: false,
            message: 'success',
            data: response?.data?.data
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  checkCarrierAssignationValidation(
    header: any,
    data: CartCarrierAssignationData
  ): Promise<any> | object {
    if (isEmpty(data)) {
      throw new Error('Payload not valid')
    }

    data.remote_ip =
      header['x-forwarded-for'] || header.connection.remoteAddress
    data.company_code = process.env.COMPANY_CODE

    if (data.customer.customer_email) {
      data.customer.customer_email = data.customer.customer_email
        .trim()
        .toLowerCase()
    }

    // Validate qty of the items if exist
    if (!isEmpty(data?.items)) {
      data?.items?.forEach((item: any) => {
        if (item.hasOwnProperty('qty_ordered')) {
          if (!isEmpty(item?.qty_ordered) && parseInt(item?.qty_ordered) < 1) {
            item.qty_ordered = 1
          }
        }
      })
    }

    return this.httpService
      .post(
        `${process.env.CART_API_URL}cart/assignation/carrier-validation`,
        { data },
        { headers: defaultHeader }
      )
      .pipe(
        map((response: any): any => {
          if (
            !isEmpty(response?.data?.errors) ||
            (response?.status === 200 && isEmpty(response?.data?.data))
          ) {
            const message: string =
              response?.data?.errors?.message ||
              'Terjadi kesalahan silakan ulangi beberapa saat lagi'

            throw new Error(message)
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(
            e,
            '',
            false,
            'Terjadi kesalahan, silahkan ulangi beberapa saat lagi'
          )

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  recreate(header: any, data: CartRecreateData) {
    if (!data) {
      const errorMessage: any = ResponseErrorMessage({}, 200, false)

      throw new HttpException(errorMessage.payload, errorMessage.status)
    }

    data.remote_ip =
      header['x-forwarded-for'] || header.connection.remoteAddress
    data.company_code = process.env.COMPANY_CODE

    if (
      data?.device_token &&
      (data?.device === 'ace-android' || data?.device === 'ace-ios')
    ) {
      data.device_token = ''
    }

    return this.httpService
      .post(`${process.env.CART_API_URL}cart/recreate`, data, {
        headers: defaultHeader
      })
      .pipe(
        map((response: any): Promise<any> | object => {
          if (
            response?.data?.errors?.code === 404 ||
            response?.data?.errors?.code === 500
          ) {
            return {
              error: response?.data?.errors,
              message: 'recreate cart failed',
              data: null
            }
          } else {
            return {
              error: null,
              message: 'recreate cart success',
              data: response?.data?.data
            }
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }
}
