import { Customer } from '../global/dto/customer.dto'

/* PROPERTY VALUE NOTES */
// 1. symbol '!' after property name means value is not null or undefined
// 2. set property initial value (sku: String = '')
// 3. inject property value through constructor

/* Contoh ValidateNested */
// @ValidateNested({ each: true })
// @Type(() => CartItem)

class CartItem {
  sku: string
  qty_ordered: any
}

class CartData {
  cart_id: string
  minicart_id: string
  items: CartItem[]
  customer: Customer
  remote_ip: string
  company_code: string
  device: string
  store_code: string
}

class CartUpdateData extends CartData {
  is_update_address: any
}

class CartRecreateData extends CartData {
  reserved_order_no: string
  device_token: string
}

class CartDeleteItemData extends CartData {}
class CartCarrierAssignationData extends CartData {}

export {
  CartUpdateData,
  CartRecreateData,
  CartDeleteItemData,
  CartCarrierAssignationData
}
