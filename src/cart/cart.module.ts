import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common'
import { HttpModule } from '@nestjs/axios'
import { JwtModule } from '@nestjs/jwt'
import { JwtVerifyAddressMiddleware } from 'src/middleware/jwt/JwtVerifyAddress.middleware'
import { CartController } from './cart.controller'
import { CartService } from './cart.service'

@Module({
  imports: [
    HttpModule.register({
      timeout: 60000
    }),
    JwtModule.registerAsync({
      useFactory: () => ({
        secret: process.env.JWT_SECRET_KEY
      })
    })
  ],
  controllers: [CartController],
  providers: [CartService]
})
export class CartModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(JwtVerifyAddressMiddleware)
      .forRoutes({ path: 'cart/update', method: RequestMethod.POST })
    consumer
      .apply(JwtVerifyAddressMiddleware)
      .forRoutes({ path: 'cart/delete', method: RequestMethod.POST })
    consumer.apply(JwtVerifyAddressMiddleware).forRoutes({
      path: 'cart/assignation/carrier-validaton',
      method: RequestMethod.POST
    })
  }
}
