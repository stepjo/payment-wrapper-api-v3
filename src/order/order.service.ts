import { Injectable, HttpException } from '@nestjs/common'
import { HttpService } from '@nestjs/axios'
import { map, catchError } from 'rxjs'
import { defaultHeader } from 'src/config/header.config'
import { CatchErrorMessage } from 'src/utils/error/ErrorMessage.utils'
import { OrderCancelData } from './order.dto'
import { isEmpty } from 'validate.js'

@Injectable({})
export class OrderService {
  constructor(private httpService: HttpService) {}

  cancelOrder(data: OrderCancelData): Promise<any> | object {
    return this.httpService
      .post(
        `${process.env.GO_ORDER_URL}sales/order/cancel`,
        { data },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response: any): Promise<any> | object => {
          const result = response?.data

          if (isEmpty(result?.errors)) {
            if (result?.messages[0] === 'success') {
              return {
                error: null,
                message: 'cancel order success',
                data: 'cancel order success'
              }
              // 200 = Midtrans, 0000 = Nicepay
            } else if (
              result?.data?.ResultCode === '200' ||
              result?.data?.ResultCode === '0000'
            ) {
              return {
                error: null,
                message: 'cancel order success',
                data: 'cancel order success'
              }
            } else {
              return {
                error: 'cancel order failed',
                message: 'cancel order failed',
                data: null
              }
            }
          } else {
            return {
              error: result?.errors?.messages[0] || 'cancel order failed',
              message: 'cancel order failed',
              data: null
            }
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(
            e,
            '',
            false,
            '',
            null,
            null
          )

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }
}
