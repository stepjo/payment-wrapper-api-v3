import { Controller, Post, Body, HttpCode } from '@nestjs/common'
import { OrderCancelData } from './order.dto'
import { OrderService } from './order.service'

@Controller('order')
export class OrderController {
  constructor(private orderService: OrderService) {}

  @Post('cancel')
  @HttpCode(200)
  cancelorder(@Body() data: OrderCancelData): Promise<any> | object {
    return this.orderService.cancelOrder(data)
  }
}
