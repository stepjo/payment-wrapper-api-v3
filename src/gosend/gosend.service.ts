// BASE
import { Injectable, HttpException } from '@nestjs/common'
import { HttpService } from '@nestjs/axios'
import { map, catchError, firstValueFrom } from 'rxjs'

// CONFIG
import { defaultHeader } from 'src/config/header.config'

// UTILS
import { CatchErrorMessage } from 'src/utils/error/ErrorMessage.utils'
import { isArray, isEmpty } from 'validate.js'
import _ from 'lodash'

// DTO
import { GosendValidateOrderData } from './gosend.dto'

const expressCourierValidate = async (
  httpService: HttpService,
  destinationGeolocation: string,
  sku: string | number,
  carrierId: number,
  customerAddressId: number,
  cartId: string
) => {
  try {
    const retreiveGeolocationResponse = await firstValueFrom(
      httpService
        .get(
          `${process.env.CART_API_URL}cart/assignation/origin?cart_id=${cartId}&sku=${sku}&customer_address_id=${customerAddressId}&carrier_id=${carrierId}&destination_geolocation=${destinationGeolocation}`
        )
        .pipe(
          map((response: any): Promise<any> => {
            return response
          }),
          catchError((e: any): Promise<any> => {
            const errorMessage: any = CatchErrorMessage(
              e,
              '',
              false,
              '',
              null,
              null
            )

            throw new HttpException(errorMessage.payload, errorMessage.status)
          })
        )
    )

    if (retreiveGeolocationResponse?.status === 200) {
      const checkGeolocationData: any = retreiveGeolocationResponse?.data

      if (!isEmpty(checkGeolocationData?.errors)) {
        // it means error ocurred
        // return res.json({error: false, message: checkGeolocationData.errors[0], data: {}})
        return {
          error: false,
          message: checkGeolocationData?.errors?.message,
          data: {}
        }
      } else {
        // if success, we need to get from gojek
        try {
          const geolocationData: any = checkGeolocationData?.data
          const checkOrderData: any = {
            data: {
              origin: geolocationData?.geolocation,
              destination: destinationGeolocation,
              payment_type: '3'
            }
          }

          const checkResponse: any = await firstValueFrom(
            httpService
              .put(
                `${process.env.SHIPMENT_API_URL}gosend/validate`,
                checkOrderData,
                { headers: defaultHeader }
              )
              .pipe(
                map((response: any): Promise<any> => {
                  return response
                }),
                catchError((e: any): Promise<any> => {
                  const errorMessage: any = CatchErrorMessage(
                    e,
                    '',
                    false,
                    '',
                    null,
                    null
                  )

                  throw new HttpException(
                    errorMessage.payload,
                    errorMessage.status
                  )
                })
              )
          )

          if (checkResponse?.status === 200) {
            const responseData: any = checkResponse?.data

            if (!isEmpty(responseData?.Data)) {
              const findInstant: number = _.findIndex(
                responseData?.Data,
                (data: any) => {
                  return data.DeliveryMethod.toLowerCase() === 'instant'
                }
              )

              if (findInstant !== -1) {
                if (
                  isEmpty(responseData?.Data[findInstant]?.Details?.errors) &&
                  isArray(responseData?.Data[findInstant]?.Details?.errors)
                ) {
                  // it means error / not available, remove from response
                  responseData?.Data?.splice(findInstant, 1)
                } else {
                  // validate distance. it should be less than 40km
                  if (
                    responseData?.Data[findInstant]?.Details?.distance &&
                    parseFloat(
                      responseData?.Data[findInstant]?.Details?.distance
                    ) > 40
                  ) {
                    responseData?.Data?.splice(findInstant, 1)
                  }
                }
              }

              // Same Day
              const findSameDay: number = _.findIndex(
                responseData?.Data,
                (data: any) => {
                  return data?.DeliveryMethod?.toLowerCase() === 'same day'
                }
              )

              if (findSameDay !== -1) {
                if (
                  isEmpty(responseData?.Data[findSameDay]?.Details?.errors) &&
                  isArray(responseData?.Data[findSameDay]?.Details?.errors)
                ) {
                  // it means error / not available, remove from response
                  responseData?.Data?.splice(findSameDay, 1)
                } else {
                  // validate distance. it should be less than 40km
                  if (
                    responseData?.Data[findSameDay]?.Details?.distance &&
                    parseFloat(
                      responseData?.Data[findSameDay]?.Details?.distance
                    ) > 40
                  ) {
                    responseData?.Data?.splice(findSameDay, 1)
                  }
                }
              }

              // return res.json({error: false, message: 'Validate Success', data: responseData.Data})
              return {
                error: false,
                message: 'Validate Success',
                data: responseData?.Data
              }
            } else {
              //
            }
          } else {
            // return res.json({error: true, message: checkResponse.problem, data: {}})
            return {
              error: true,
              message: checkResponse?.problem,
              data: {}
            }
          }
        } catch (e) {
          return {
            error: true,
            message: e?.message,
            data: {}
          }
        }
      }
    } else {
      const checkGeolocationData: any = retreiveGeolocationResponse?.data

      if (isEmpty(checkGeolocationData?.errors)) {
        return {
          error: true,
          message: checkGeolocationData?.errors?.message,
          data: {}
        }
      }
    }
  } catch (e) {
    return {
      error: true,
      message: e?.message,
      data: {}
    }
  }
}

@Injectable({})
export class GosendService {
  constructor(private httpService: HttpService) {}

  async validateOrder(data: GosendValidateOrderData) {
    const { data: payload }: any = data

    const addressId: string = payload?.address_id
    const sku: string | number = payload?.sku
    const cartId: string = payload?.cart_id
    const destinationGeolocation: string | number =
      payload?.destination_geolocation
    const carrierId: string | number = payload?.carrier_id

    if (addressId) {
      try {
        // first, go to API PHP to check store's geolocation based on kecamatan_id
        const data = await expressCourierValidate(
          this.httpService,
          destinationGeolocation,
          sku,
          carrierId,
          addressId,
          cartId
        )

        return {
          data
        }
      } catch (e: any) {
        const errorMessage: any = CatchErrorMessage(
          e,
          '',
          false,
          '',
          null,
          null
        )

        throw new HttpException(errorMessage.payload, errorMessage.status)
      }
    }

    return {}
  }
}
