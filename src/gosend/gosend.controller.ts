// BASE
import { Controller, Post, Body, HttpCode } from '@nestjs/common'

// DTO
import { GosendValidateOrderData } from './gosend.dto'

// SERVICE
import { GosendService } from './gosend.service'

@Controller('gosend')
export class GosendController {
  constructor(private gosendService: GosendService) {}

  @Post('validate')
  @HttpCode(200)
  checkCarrierAssignationValidation(@Body() data: GosendValidateOrderData) {
    return this.gosendService.validateOrder(data)
  }
}
