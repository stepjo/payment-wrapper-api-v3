// BASE
import { Module } from '@nestjs/common'
import { HttpModule } from '@nestjs/axios'

// CONTROLLER
import { GosendController } from './gosend.controller'

// SERVICE
import { GosendService } from './gosend.service'

@Module({
  imports: [
    HttpModule.register({
      timeout: 60000
    })
  ],
  controllers: [GosendController],
  providers: [GosendService]
})
export class GosendModule {}
