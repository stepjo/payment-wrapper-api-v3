class GosendValidateOrderData {
  address_id: number
  sku: string | number
  cart_id: string
  destination_geolocation: string
  shipping_phone: string
  carrier_id: number
}

export { GosendValidateOrderData }
