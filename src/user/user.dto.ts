import { Customer } from 'src/global/dto/customer.dto'
import { City, District, Province } from 'src/global/dto/misc.dto'

// REQUEST
class UserBaseReq {
  customer_id: string | number
}

// DATA
class UserBaseData {
  first_name: string
  last_name: string
  phone: string | number
  full_address: string
  post_code: string | number
  province: Province
  city: City
  kecamatan: District
  address_name: string
  geolocation: string
  address_id: string | number
  customer: Customer
}

class UserBaseOtpData {
  otp_number?: number
  customer_id: string | number
}

class UserGenerateOtpData extends UserBaseOtpData {
  action: string
  send_to: string
  phone: string
  email: string
  new_phone: string
  new_email: string
}

class UserOtpInformaData extends UserBaseOtpData {
  skip_cart: boolean
  point_redeem: number
  cart_id: string
  otp: number
}

class UserAddressUpdateData extends UserBaseData {
  customer_address_id: string | number
  cart_id: string
}

// RESPONSE
class UserBaseResponse {
  constructor(
    data: object | Array<any> = [],
    error: boolean = false,
    msg: string = ''
  ) {
    this.data = data
    this.error = error
    this.msg = msg
  }

  data: object | Array<any>
  error: boolean
  msg?: string
}

class UserAddressListResponse extends UserBaseResponse {
  data: Array<UserAddressListResponseData>
}

class UserAddressListResponseData {
  address_id: string
}

class UserOtpResponse extends UserBaseResponse {
  constructor(
    data: object | Array<any> = [],
    error: boolean = false,
    msg: string = '',
    message: string
  ) {
    super(data, error, msg)

    this.message = message
  }

  message: string
}

class UserCustomerMemberResponse extends UserBaseResponse {
  constructor(
    data: object | Array<any> = [],
    error: boolean = false,
    msg: string = '',
    message: string
  ) {
    super(data, error, msg)

    this.message = message
  }

  message: string
}

export {
  UserBaseReq,
  UserBaseData,
  UserAddressUpdateData,
  UserGenerateOtpData,
  UserOtpInformaData,
  UserBaseResponse,
  UserAddressListResponse,
  UserOtpResponse,
  UserCustomerMemberResponse
}
