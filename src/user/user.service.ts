// BASE
import { Injectable, HttpException } from '@nestjs/common'
import { HttpService } from '@nestjs/axios'
import { map, catchError } from 'rxjs'

// CONFIG
import {
  customerHeader,
  defaultHeader,
  tpsHeader
} from 'src/config/header.config'

// UTILS
import {
  CatchErrorMessage,
  ResponseErrorMessage
} from 'src/utils/error/ErrorMessage.utils'
import { CheckErrorResponse } from 'src/utils/error/ErrorResponse.utils'
import { addressConstraints } from 'src/utils/validation/ValidateConstraint.utils'
import { isEmpty, validate } from 'validate.js'
import moment from 'moment'

// DTO
import {
  UserBaseData,
  UserAddressUpdateData,
  UserGenerateOtpData,
  UserOtpInformaData,
  UserBaseResponse,
  UserAddressListResponse,
  UserOtpResponse,
  UserCustomerMemberResponse,
  UserBaseReq
} from './user.dto'

@Injectable({})
export class UserService {
  constructor(private httpService: HttpService) {}

  addressList(req: UserBaseReq) {
    return this.httpService
      .get(`${process.env.API_URL}customers/address/`, {
        headers: customerHeader(req?.customer_id)
      })
      .pipe(
        map((response) => {
          const addressListResponse = new UserAddressListResponse()

          if (CheckErrorResponse(response?.data?.errors)) {
            if (response?.data?.errors?.code === 'RR302') {
              return addressListResponse
            } else {
              const errorMessage: any = ResponseErrorMessage(
                response,
                404,
                false
              )

              throw new HttpException(errorMessage.payload, errorMessage.status)
            }
          }

          const { data } = response?.data

          const mutatedData: any = data?.reduce((prev: any, current: any) => {
            current.address_id = parseInt(current?.address_id)

            return prev.concat(current)
          }, [])

          addressListResponse.data = mutatedData

          return addressListResponse
        }),
        catchError((e) => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  addressCreate(body: any) {
    const { data: payload } = body

    const hasError = validate(payload, addressConstraints)

    if (hasError) {
      const errorMessage: any = ResponseErrorMessage({}, 200, false, hasError)

      throw new HttpException(errorMessage.payload, errorMessage.status)
    }

    const {
      first_name,
      last_name,
      phone,
      full_address,
      post_code,
      province,
      city,
      kecamatan,
      address_name,
      geolocation,
      customer
    }: UserBaseData = payload

    const data: object = {
      customer_id: customer?.customer_id,
      country: {
        country_id: '102',
        country_code: 'ID',
        country_name: 'Indonesia'
      },
      kelurahan: {
        kelurahan_id: '1',
        kelurahan_name: 'Indonesia Raya Sehat Bangsaku'
      },
      address_type: 'shipping',
      status: '10',
      first_name,
      last_name,
      phone,
      full_address: full_address?.replace(/["',&*]/g, ''),
      post_code,
      province: { province_id: province?.province_id },
      city: { city_id: city?.city_id },
      kecamatan: { kecamatan_id: kecamatan?.kecamatan_id },
      address_name: address_name?.replace(/["']/g, ''),
      geolocation
    }

    return this.httpService
      .post(
        `${process.env.API_URL}customers/address/`,
        { data },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, 404, false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          return new UserBaseResponse(
            Object.assign({}, payload, response?.data?.data),
            false,
            'Address saved'
          )
        }),
        catchError((e) => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  addressUpdate(body: any) {
    const { data: payload } = body

    const hasError = validate(payload, addressConstraints)

    if (hasError) {
      const errorMessage: any = ResponseErrorMessage({}, 200, false, hasError)

      throw new HttpException(errorMessage.payload, errorMessage.status)
    }

    const {
      first_name,
      last_name,
      phone,
      full_address,
      post_code,
      province,
      city,
      kecamatan,
      address_name,
      geolocation,
      customer,
      address_id
    }: UserAddressUpdateData = payload

    const data: object = {
      customer_id: customer?.customer_id,
      address_type: 'shipping',
      status: '10',
      first_name,
      last_name,
      phone,
      full_address: full_address?.replace(/["',&*]/g, ''),
      post_code,
      province: { province_id: province?.province_id },
      city: { city_id: city?.city_id },
      kecamatan: { kecamatan_id: kecamatan?.kecamatan_id },
      address_name: address_name?.replace(/["']/g, ''),
      geolocation
    }

    return this.httpService
      .put(
        `${process.env.API_URL}customers/address/${address_id}`,
        { data },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, 404, false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          return new UserBaseResponse(payload)
        }),
        catchError((e) => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  aa2AddressUpdate(body: any) {
    const { data: payload } = body

    const hasError = validate(payload, addressConstraints, { format: 'custom' })

    if (hasError) {
      const errorMessage: any = ResponseErrorMessage({}, 200, false, hasError)

      throw new HttpException(errorMessage.payload, errorMessage.status)
    }

    const customerID = payload?.customer?.customer_id

    const {
      first_name,
      last_name,
      phone,
      full_address,
      post_code,
      province,
      city,
      kecamatan,
      address_name,
      geolocation,
      customer_address_id,
      address_id,
      cart_id
    }: UserAddressUpdateData = payload

    const data: object = {
      items: [
        {
          shipping_address: {
            customer: {
              customer_id: customerID
            },
            address_id,
            customer_address_id,
            first_name,
            last_name,
            full_address: full_address.replace(/["',&*]/g, ''),
            phone,
            post_code,
            province: { province_id: province.province_id.toString() },
            city: { city_id: city.city_id.toString() },
            kecamatan: { kecamatan_id: kecamatan.kecamatan_id.toString() },
            address_name: address_name.replace(/["']/g, ''),
            geolocation
          }
        }
      ],
      cart_id
    }

    return this.httpService
      .put(
        `${process.env.CART_API_URL}cart/update/simplify_address`,
        { data },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, 404, false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          return new UserBaseResponse(payload)
        }),
        catchError((e) => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  generateOtp(req: UserBaseReq, body: any) {
    const { data: payload } = body

    const data: UserGenerateOtpData = {
      action: payload?.action || '',
      customer_id: req?.customer_id || '',
      send_to: payload?.send_to || '',
      phone: payload?.phone || '',
      email: payload?.email || '',
      otp_number: payload?.otp_number || '',
      new_phone: payload?.new_phone || '',
      new_email: payload?.new_email || ''
    }

    return this.httpService
      .post(
        `${process.env.API_URL}ace/customer/generate_otp`,
        { data },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response) => {
          const generateOtpResponseFailed = new UserOtpResponse(
            {},
            true,
            '',
            response?.data?.message
          )
          const generateOtpResponseSuccess = new UserOtpResponse(
            response?.data?.data,
            false,
            '',
            response?.data?.message
          )

          if (response?.status === 200) {
            if (!isEmpty(response?.data?.errors)) {
              generateOtpResponseFailed.message =
                response?.data?.errors?.messages || 'Data tidak ditemukan'

              return generateOtpResponseFailed
            } else {
              return generateOtpResponseSuccess
            }
          }

          return generateOtpResponseFailed
        }),
        catchError((e): any => {
          if (e?.response?.status === 400 && e?.response?.data?.errors) {
            return new UserOtpResponse(
              {},
              true,
              '',
              e?.response?.data?.errors?.messages
            )
          } else {
            const errorMessage: any = CatchErrorMessage(e, '', false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }
        })
      )
  }

  getCustomerMember(req: UserBaseReq) {
    const dateNow: string = moment().format('YYYY-MM-DD 00:00:00')
    const ahiBlockedStartDateRedeemPoint =
      process.env.AHI_BLOCKED_START_DATE_REDEEM_POINT
    const ahiBlockedEndDateRedeemPoint =
      process.env.AHI_BLOCKED_END_DATE_REDEEM_POINT

    if (
      ahiBlockedStartDateRedeemPoint !== '' &&
      ahiBlockedEndDateRedeemPoint !== '' &&
      dateNow >= ahiBlockedStartDateRedeemPoint &&
      dateNow <= ahiBlockedEndDateRedeemPoint
    ) {
      return new UserCustomerMemberResponse(
        {},
        true,
        '',
        'Maaf, saat ini sedang ada perbaikan sistem pada fitur redeem point'
      )
    }

    return this.httpService
      .get(`${process.env.API_URL}ace/customer/${req?.customer_id}`, {
        headers: customerHeader(req?.customer_id)
      })
      .pipe(
        map((response) => {
          const customerMemberResponse = new UserCustomerMemberResponse(
            {},
            true,
            '',
            ''
          )

          if (response?.status === 200) {
            if (!isEmpty(response?.data?.errors)) {
              customerMemberResponse.message =
                response?.data?.errors?.messages || 'Data tidak ditemukan'

              return customerMemberResponse
            } else {
              const responseData: any = response?.data?.data

              if (responseData?.total_point) {
                responseData.point = responseData?.total_point.substr(
                  0,
                  responseData?.total_point.indexOf(' ')
                )
              }

              // BASE POINT
              responseData.base_point_value = 2500

              customerMemberResponse.data = responseData
              customerMemberResponse.message = response?.data?.message

              return customerMemberResponse
            }
          }

          customerMemberResponse.message = 'failed'

          return customerMemberResponse
        }),
        catchError((e): any => {
          if (e?.response?.status === 400 && e?.response?.data?.errors) {
            return new UserCustomerMemberResponse(
              {},
              true,
              '',
              e?.response?.data?.errors?.message
            )
          } else {
            const errorMessage: any = CatchErrorMessage(e, '', false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }
        })
      )
  }

  getMemberTieringInforma(req: UserBaseReq) {
    const dateNow: string = moment().format('YYYY-MM-DD 00:00:00')
    const hciBlockedStartDateRedeemPoint =
      process.env.HCI_BLOCKED_START_DATE_REDEEM_POINT
    const hciBlockedEndDateRedeemPoint =
      process.env.HCI_BLOCKED_END_DATE_REDEEM_POINT

    if (
      hciBlockedStartDateRedeemPoint !== '' &&
      hciBlockedEndDateRedeemPoint !== '' &&
      dateNow >= hciBlockedStartDateRedeemPoint &&
      dateNow <= hciBlockedEndDateRedeemPoint
    ) {
      throw new Error(
        'Maaf, saat ini sedang ada perbaikan sistem pada fitur Redeem Point'
      )
    }

    return this.httpService
      .get(
        `${process.env.API_URL}informa/customer/tiering/${req?.customer_id}`,
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response: any): any => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, '', true)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          } else {
            if (response?.status === 200) {
              // BASE POINT
              response.data.data.base_point_value = 2500

              return new UserCustomerMemberResponse(
                response?.data?.data,
                true,
                '',
                'success'
              )
            }
          }
        }),
        catchError((e): any => {
          if (e?.response?.status === 400 && e?.response?.data?.errors) {
            return new UserCustomerMemberResponse(
              {},
              true,
              '',
              e?.response?.data?.errors?.message
            )
          } else {
            const errorMessage: any = CatchErrorMessage(e, '', false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }
        })
      )
  }

  submitOtpInforma(req: UserBaseReq, body: UserOtpInformaData) {
    const data: UserOtpInformaData = {
      customer_id: req?.customer_id,
      cart_id: !body?.skip_cart ? body?.cart_id : '',
      point_redeem: body?.point_redeem,
      otp: body?.otp_number,
      skip_cart: body?.skip_cart || false
    }

    return this.httpService
      .post(
        `${process.env.API_URL}informa/customer/pwp/submit_otp`,
        { data },
        {
          headers: tpsHeader
        }
      )
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, '', true)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          return new UserOtpResponse(response?.data?.data, false, '', 'success')
        }),
        catchError((e): any => {
          if (e?.response?.status === 400 && e?.response?.data?.errors) {
            return new UserOtpResponse(
              {},
              true,
              '',
              e?.response?.data?.errors?.messages
            )
          } else {
            const errorMessage: any = CatchErrorMessage(e, '', false, '')

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }
        })
      )
  }

  sendOtpInforma(req: UserBaseReq, data: UserOtpInformaData) {
    data.customer_id = req?.customer_id
    data.skip_cart = data?.skip_cart || false
    data.cart_id = data?.skip_cart ? data?.cart_id : ''

    return this.httpService
      .post(
        `${process.env.API_URL}informa/customer/pwp/send_otp`,
        { data },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const error = response?.data?.errors?.messages

            if (!isEmpty(response?.data?.data)) {
              return new UserOtpResponse(response?.data?.data, true, '', error)
            }

            const errorMessage: any = ResponseErrorMessage(response, '', true)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          return new UserOtpResponse(response?.data?.data, false, '', 'success')
        }),
        catchError((e): any => {
          if (e?.response?.status === 400 && e?.response?.data?.errors) {
            return new UserOtpResponse(
              {},
              true,
              '',
              e?.response?.data?.errors?.messages
            )
          } else {
            const errorMessage: any = CatchErrorMessage(e, '', false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }
        })
      )
  }
}
