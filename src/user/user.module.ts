// BASE
import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common'
import { HttpModule } from '@nestjs/axios'
import { JwtModule } from '@nestjs/jwt'

// MIDDLEWARE
import { JwtVerifyAddressMiddleware } from 'src/middleware/jwt/JwtVerifyAddress.middleware'

// CONTROLLER
import { UserController } from './user.controller'

// SERVICE
import { UserService } from './user.service'

@Module({
  imports: [
    HttpModule.register({
      timeout: 60000
    }),
    JwtModule.registerAsync({
      useFactory: () => ({
        secret: process.env.JWT_SECRET_KEY
      }),
      imports: undefined
    })
  ],
  controllers: [UserController],
  providers: [UserService]
})
export class UserModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(JwtVerifyAddressMiddleware)
      .exclude({ path: 'user/member-chances', method: RequestMethod.GET })
  }
}
