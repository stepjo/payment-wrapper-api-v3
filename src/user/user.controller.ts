// BASE
import { Controller, Get, Post, Body, HttpCode, Req } from '@nestjs/common'

// DTO
import {
  UserBaseReq,
  UserBaseData,
  UserAddressUpdateData,
  UserGenerateOtpData,
  UserOtpInformaData
} from './user.dto'

// SERVICE
import { UserService } from './user.service'

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Get('address-list')
  addressList(@Req() req: UserBaseReq) {
    return this.userService.addressList(req)
  }

  @Post('address')
  @HttpCode(200)
  addressCreate(@Body() data: UserBaseData) {
    return this.userService.addressCreate(data)
  }

  @Post('address/:id')
  @HttpCode(200)
  address(@Body() data: UserAddressUpdateData) {
    return this.userService.addressUpdate(data)
  }

  @Post('simplify-address')
  @HttpCode(200)
  aa2AddressUpdate(@Body() data: UserAddressUpdateData) {
    return this.userService.aa2AddressUpdate(data)
  }

  @Post('ace/generate-otp')
  @HttpCode(200)
  generateOtp(@Req() req: UserBaseReq, @Body() data: UserGenerateOtpData) {
    return this.userService.generateOtp(req, data)
  }

  @Get('ace/customer-member')
  getCustomerMember(@Req() req: UserBaseReq) {
    return this.userService.getCustomerMember(req)
  }

  @Get('informa/member-tiering')
  getMemberTieringInforma(@Req() req: UserBaseReq) {
    return this.userService.getMemberTieringInforma(req)
  }

  @Post('informa/submit-otp')
  @HttpCode(200)
  submitOtpInforma(@Req() req: UserBaseReq, @Body() data: UserOtpInformaData) {
    return this.userService.submitOtpInforma(req, data)
  }

  @Post('informa/send-otp')
  @HttpCode(200)
  sendOtpInforma(@Req() req: UserBaseReq, @Body() data: UserOtpInformaData) {
    return this.userService.sendOtpInforma(req, data)
  }
}
