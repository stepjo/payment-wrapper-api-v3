import { Payment, PaymentVa } from 'src/global/dto/payment.dto'

// PARAM
class PaymentBaseParam {
  cartId: string
}

class PaymentStatusParam extends PaymentBaseParam {
  counter: number
}

class PaymentListBinCCParam {
  bin: number
}

// DATA
class PaymentBaseData {
  constructor(cart_id: string, payment: Payment) {
    this.cart_id = cart_id
    this.payment = payment
  }

  cart_id: string
  payment: Payment | object
  save_promotion: boolean
}

class PaymentOvoData extends PaymentBaseData {
  constructor(cart_id: string, payment: Payment, no_hp: string | number) {
    super(cart_id, payment)

    this.no_hp = no_hp
  }

  no_hp: string | number
}

class PaymentGopayData extends PaymentBaseData {
  constructor(
    cart_id: string,
    payment: Payment,
    requester: string,
    save_promotion: boolean
  ) {
    super(cart_id, payment)

    this.requester = requester
    this.save_promotion = save_promotion
  }

  requester: string
  save_promotion: boolean
}

class PaymentSubmitData extends PaymentBaseData {
  constructor(cart_id: string, payment: PaymentVa) {
    super(cart_id, payment)
  }

  payment: PaymentVa
}

class PaymentCheckData extends PaymentBaseData {
  constructor(cart_id: string, payment: PaymentVa) {
    super(cart_id, payment)
  }

  payment: PaymentVa
}

class PaymentTrackingData extends PaymentBaseData {
  constructor(
    cart_id: string,
    payment: Payment,
    type: string,
    bank_issuing: string,
    cicilan: string | number,
    order_no: string 
  ) {
    super(cart_id, payment)

    this.type = type
    this.bank_issuing = bank_issuing
    this.cicilan = cicilan
    this.order_no = order_no
  }

  type: string
  bank_issuing: string
  cicilan: string | number
  order_no: string
}

class PaymentGetSavedCreditCardsRequest {
  customer_id: string
}

// RESPONSE
class PaymentBaseResponse {
  constructor(data: object | Array<any>, error: boolean) {
    this.data = data
    this.error = error
  }

  data: object | Array<any>
  error: boolean
}

export {
  PaymentBaseParam,
  PaymentStatusParam,
  PaymentListBinCCParam,
  PaymentBaseData,
  PaymentOvoData,
  PaymentGopayData,
  PaymentSubmitData,
  PaymentCheckData,
  PaymentTrackingData,
  PaymentGetSavedCreditCardsRequest,
  PaymentBaseResponse
}
