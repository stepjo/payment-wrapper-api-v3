// BASE
import { Injectable, HttpException } from '@nestjs/common'
import { HttpService } from '@nestjs/axios'
import { map, catchError } from 'rxjs'

// DTO
import {
  PaymentBaseData,
  PaymentOvoData,
  PaymentGopayData,
  PaymentCheckData,
  PaymentSubmitData,
  PaymentTrackingData,
  PaymentGetSavedCreditCardsRequest,
  PaymentBaseResponse,
  PaymentBaseParam,
  PaymentStatusParam,
  PaymentListBinCCParam,
} from './payment.dto'

// CONFIG
import { cartNoRefreshHeader, defaultHeader } from 'src/config/header.config'

// UTILS
import {
  CatchErrorMessage,
  ResponseErrorMessage
} from 'src/utils/error/ErrorMessage.utils'
import { CheckErrorResponse } from 'src/utils/error/ErrorResponse.utils'
import moment from 'moment'

@Injectable({})
export class PaymentService {
  constructor(private httpService: HttpService) {}

  paymentList(param: PaymentBaseParam) {
    return this.httpService
      .get(
        `${process.env.PAYMENT_API_URL}payment_list?cart_id=${param?.cartId}`,
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, 200, false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          return new PaymentBaseResponse(response?.data?.data, false)
        }),
        catchError((e) => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  submitPaymentOvo(body: any) {
    let { data } = body

    data = new PaymentOvoData(
      data?.cart_id,
      {
        method: data?.method,
        save_promotion: data?.save_promotion
      },
      data?.no_hp
    )

    return this.httpService
      .post(
        `${process.env.PAYMENT_API_URL_V2}ovo/push`,
        { data },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, 200, false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          return new PaymentBaseResponse(response?.data?.data, false)
        }),
        catchError((e) => {
          const errorMessage: any = CatchErrorMessage(e, '', true)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  submitPaymentGopay(body: any) {
    let { data } = body

    data = new PaymentGopayData(
      data?.cart_id,
      {},
      data?.requester,
      data?.save_promotion
    )

    return this.httpService
      .post(
        `${process.env.PAYMENT_API_URL_V2}midtrans/insert`,
        { data },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, 200, false)

            throw new HttpException(errorMessage, 200)
          }

          return new PaymentBaseResponse(response?.data?.data, false)
        }),
        catchError((e) => {
          const errorMessage: any = CatchErrorMessage(e, '', true)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  submitPayment(body: any) {
    let { data } = body
    const { payment }: PaymentSubmitData = data

    data = new PaymentSubmitData(data.cart_id, {
      bank_transfer: data?.payment?.bank_transfer,
      method: data?.payment?.method,
      va_bank: data?.payment?.va_bank
    })

    return this.httpService
      .post(
        `${process.env.PAYMENT_API_URL}shopping_cart/save`,
        { data },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(
              response,
              200,
              payment?.method === 'vpayment'
            )

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          return new PaymentBaseResponse(response?.data?.data, false)
        }),
        catchError((e) => {
          const errorMessage: any = CatchErrorMessage(e, '', true)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  paymentCheck(body: any) {
    let { data } = body

    data = new PaymentCheckData(data?.cart_id, {
      method: data?.payment?.method,
      va_bank: data?.payment?.va_bank,
      save_promotion: false
    })

    data.payment.cc_number = data.payment.card_number

    // payload.cart_id = '052f7c3213d01e246c796658fd02cfa4a722a3f2'

    return this.httpService
      .post(`${process.env.CART_API_URL}cart/payment/check`, data, {
        headers: defaultHeader
      })
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, 200, false)

            throw new HttpException(errorMessage.payload, 200)
          }

          return new PaymentBaseResponse(response?.data?.data, false)
        }),
        catchError((e) => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  submitCtccRedirectPayment(body: any) {
    let { data } = body

    data = new PaymentBaseData(data?.cart_id, {
      method: data?.payment?.method
    })

    return this.httpService
      .post(
        `${process.env.PAYMENT_API_URL_V2}nicepay/insert`,
        { data },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, '', false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          if (response?.status === 200) {
            return new PaymentBaseResponse(response?.data?.data, false)
          }

          const errorMessage: any = ResponseErrorMessage(response, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        }),
        catchError((e) => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  statusPayment(param: PaymentStatusParam) {
    if (!param?.counter) {
      param.counter = 0
    }

    return this.httpService
      .get(
        `${process.env.PAYMENT_API_URL}status?token=${param?.cartId}&counter=${param.counter}`,
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response) => {
          if (response?.data) {
            if (CheckErrorResponse(response?.data?.errors)) {
              if (response?.status === 201 || param?.counter === 1) {
                const errorMessage: any = ResponseErrorMessage(
                  response,
                  201,
                  false
                )

                throw new HttpException(
                  errorMessage.payload,
                  errorMessage.status
                )
              }
            }

            if (response?.status === 200) {
              return new PaymentBaseResponse(response?.data?.data, false)
            }

            if (response?.status === 500) {
              const errorMessage: any = ResponseErrorMessage(
                response,
                500,
                false
              )

              throw new HttpException(errorMessage.payload, errorMessage.status)
            }

            if (response?.status === 501) {
              const errorMessage: any = ResponseErrorMessage(
                response,
                501,
                false
              )

              throw new HttpException(errorMessage.payload, errorMessage.status)
            }

            if (response?.status === 408) {
              const errorMessage: any = ResponseErrorMessage(
                response,
                408,
                false
              )

              throw new HttpException(errorMessage.payload, errorMessage.status)
            }
          }

          const errorMessage: any = ResponseErrorMessage(
            response,
            204,
            false,
            'No Content'
          )

          throw new HttpException(errorMessage.payload, errorMessage.status)
        }),
        catchError((e: any): any => {
          if (e?.response?.status === 408 && param?.counter < 0) {
            param.counter++

            this.statusPayment(param)
          } else {
            const errorMessage: any = CatchErrorMessage(e, '', true)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }
        })
      )
  }

  checkPaymentOvo(param: PaymentBaseParam) {
    return this.httpService
      .get(
        `${process.env.PAYMENT_API_URL_V2}ovo/status?cart_id=${param?.cartId}`,
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, 200, false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          return new PaymentBaseResponse(response?.data?.data, false)
        }),
        catchError((e) => {
          const errorMessage: any = CatchErrorMessage(e, '', true)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  checkPaymentGopay(param: PaymentBaseParam) {
    return this.httpService
      .get(
        `${process.env.PAYMENT_API_URL_V2}midtrans/status/${param?.cartId}`,
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, 200, false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          return new PaymentBaseResponse(response?.data?.data, false)
        }),
        catchError((e)  => {
          const errorMessage: any = CatchErrorMessage(e, '', true)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  listBinCC(param: PaymentListBinCCParam) {
    return this.httpService
      .get(`${process.env.PAYMENT_API_URL}bin?bin=${param?.bin}`, {
        headers: defaultHeader
      })
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, 200, false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          return new PaymentBaseResponse(response?.data?.data, false)
        }),
        catchError((e) => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  tracking(body: any)  {
    let { data } = body

    data = new PaymentTrackingData(
      data.cart_id, 
      {}, 
      data?.type,
      data?.bank_issuing,
      data?.cicilan,
      data?.order_no
    )

    payload = {
      ...payload,
      created_at: moment().format('YYYY-MM-DD HH:mm:ss')
    }

    this.httpService
      .get(`${process.env.CART_API_URL}cart/${payload?.cart_id}`, {
        headers: cartNoRefreshHeader
      })
      .pipe(
        map((response: any): Promise<any> | any => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, 200, false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          if (response?.status === 200) {
            return this.httpService
              .post(
                `${process.env.PAYMENT_API_URL}tracking/save/`,
                { data: payload },
                {
                  headers: defaultHeader
                }
              )
              .pipe(
                map((response: any): Promise<any> | object => {
                  if (CheckErrorResponse(response?.data?.errors)) {
                    const errorMessage: any = ResponseErrorMessage(
                      response,
                      404,
                      false
                    )

                    throw new HttpException(
                      errorMessage.payload,
                      errorMessage.status
                    )
                  }

                  if (response?.status === 200) {
                    const data: object = response?.data?.data

                    return {
                      error: false,
                      data
                    }
                  } else {
                    const errorMessage: any = ResponseErrorMessage(
                      response,
                      200,
                      false
                    )

                    throw new HttpException(
                      errorMessage.payload,
                      errorMessage.status
                    )
                  }
                }),
                catchError((e: any): Promise<any> => {
                  const errorMessage: any = CatchErrorMessage(e, '', true)

                  throw new HttpException(
                    errorMessage.payload,
                    errorMessage.status
                  )
                })
              )
          } else {
            const errorMessage: any = ResponseErrorMessage(response, '', false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', true)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  getSavedCreditCards(
    req: PaymentGetSavedCreditCardsRequest
  ): Promise<any> | object {
    if (!req.customer_id) {
      return {
        error: true,
        message: 'no customer id given'
      }
    }

    return this.httpService
      .get(`${process.env.PAYMENT_API_URL}customer_credit_card`, {
        params: {
          customer_id: req.customer_id
        },
        headers: defaultHeader
      })
      .pipe(
        map((response: any): Promise<any> | object => {
          const errors = response?.data?.errors

          if (errors && CheckErrorResponse(errors) && errors?.code !== '404') {
            const errorMessage: any = ResponseErrorMessage(response, 200, false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          if (response?.status === 200) {
            const data: any = response?.data?.data

            return {
              error: false,
              data
            }
          } else {
            const errorMessage: any = ResponseErrorMessage(response, 200, false)

            throw new HttpException(errorMessage.paylad, errorMessage.status)
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  submitPaymentQris(data: PaymentQrisData): Promise<any> | object {
    const { data: payload }: any = data

    return this.httpService
      .post(
        `${process.env.PAYMENT_API_URL_V2}midtrans/insert/qris`,
        { data: payload },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, 200, false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          const data: object = response?.data?.data

          return {
            error: false,
            data
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', true)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  checkPaymentQris(param: PaymentCheckQrisParam): Promise<any> | object {
    return this.httpService
      .get(
        `${process.env.PAYMENT_API_URL_V2}midtrans/status/${param?.cartId}`,
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, 200, false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          const data: object = response?.data?.data

          return {
            error: false,
            data
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', true)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  changePayment(data: PaymentChangeData): Promise<any> | object {
    return this.httpService
      .put(`${process.env.PAYMENT_API_URL_V2}payment/change/submit`, data, {
        headers: defaultHeader
      })
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, 200, false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          const data: object = response?.data?.data

          return {
            error: false,
            data
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', true)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }
}
