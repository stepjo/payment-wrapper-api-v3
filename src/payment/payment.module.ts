// BASE
import { Module } from '@nestjs/common'
import { HttpModule } from '@nestjs/axios'

// CONTROLLER
import { PaymentController } from './payment.controller'

// SERVICE
import { PaymentService } from './payment.service'

@Module({
  imports: [
    HttpModule.register({
      timeout: 60000
    })
  ],
  controllers: [PaymentController],
  providers: [PaymentService]
})
export class PaymentModule {}
