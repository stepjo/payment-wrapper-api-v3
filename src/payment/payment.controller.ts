// BASE
import {
  Controller,
  Get,
  Post,
  Put,
  Body,
  Param,
  Req,
  HttpCode
} from '@nestjs/common'

// DTO
import {
  PaymentBaseData,
  PaymentOvoData,
  PaymentGopayData,
  PaymentCheckData,
  PaymentSubmitData,
  PaymentTrackingData,
  PaymentGetSavedCreditCardsRequest,
  PaymentBaseParam,
  PaymentStatusParam,
} from './payment.dto'
import { PaymentService } from './payment.service'

@Controller('payment')
export class PaymentController {
  constructor(private paymentService: PaymentService) {}

  @Get('payment-list/:cartId')
  paymentList(@Param() param: PaymentBaseParam) {
    return this.paymentService.paymentList(param)
  }

  @Post('save/ovo')
  @HttpCode(200)
  submitPaymentOvo(@Body() body: PaymentOvoData) {
    return this.paymentService.submitPaymentOvo(body)
  }

  @Post('save/gopay')
  @HttpCode(200)
  submitPaymentGopay(@Body() body: PaymentGopayData) {
    return this.paymentService.submitPaymentGopay(body)
  }

  @Post('save')
  @HttpCode(200)
  submitPayment(@Body() body: PaymentSubmitData) {
    return this.paymentService.submitPayment(body)
  }

  @Post('payment-check')
  @HttpCode(200)
  paymentCheck(@Body() body: PaymentCheckData) {
    return this.paymentService.paymentCheck(body)
  }

  // @Post('save-payment')
  // @HttpCode(200)
  // submitRedirectPayment(@Body() data: PaymentRedirectData) {
  //   return this.paymentService.submitRedirectPayment(data)
  // }

  @Post('save-payment-ctcc')
  @HttpCode(200)
  submitCtccRedirectPayment(@Body() body: PaymentBaseData) {
    return this.paymentService.submitCtccRedirectPayment(body)
  }

  @Get('status/:cartId')
  statusPayment(@Param() param: PaymentStatusParam) {
    return this.paymentService.statusPayment(param)
  }

  @Get('ovo/:cartId')
  checkPaymentOvo(@Param() param: PaymentBaseParam) {
    return this.paymentService.checkPaymentOvo(param)
  }

  @Get('gopay/:cartId')
  checkPaymentGopay(@Param() param: PaymentBaseParam) {
    return this.paymentService.checkPaymentGopay(param)
  }

  @Get('bin/:bin')
  listBinCC(@Param() param: PaymentBaseParam) {
    return this.paymentService.listBinCC(param)
  }

  @Post('tracking')
  tracking(@Body() body: PaymentTrackingData) {
    return this.paymentService.tracking(body)
  }

  @Get('saved-credit-cards')
  getSavedCreditCards(@Req() req: PaymentGetSavedCreditCardsRequest) {
    return this.paymentService.getSavedCreditCards(req)
  }

  @Post('save/qris')
  @HttpCode(200)
  submitPaymentQris(@Body() data: PaymentBaseData) {
    return this.paymentService.submitPaymentQris(data)
  }

  @Get('qris/:cartId')
  checkPaymentQris(@Param() param: PaymentBaseParam) {
    return this.paymentService.checkPaymentQris(param)
  }

  @Put('change/submit')
  changePayment(@Body() data: PaymentChangeData) {
    return this.paymentService.changePayment(data)
  }
}
