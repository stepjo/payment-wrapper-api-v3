// BASIC
import { Injectable, NestMiddleware } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { Request, Response } from 'express'

// DTO
import { UnauthorizedResponse } from './jwt.dto'

@Injectable({})
export class JwtVerifyAddressMiddleware implements NestMiddleware {
  constructor(private jwtService: JwtService) {}

  async use(req: Request, res: Response, next: Function): Promise<any> {
    res.statusCode = 200

    const unauthorizedResponse = new UnauthorizedResponse()

    const token: string =
      req?.query?.customer_id ||
      req?.body?.customer?.customer_id ||
      req?.body?.data?.customer?.customer_id ||
      req?.get('Authorization')

    if (token) {
      try {
        const decoded = await this.jwtService.verifyAsync(token)

        if (decoded?.iss === process.env.JWT_ISSUER) {
          const customerID =
            decoded?.customer_id >= 0
              ? decoded?.customer_id
              : decoded?.customerId

          req.customer_id = parseInt(customerID)

          return next()
        }

        return res.json(unauthorizedResponse)
      } catch (e) {
        console.log('JWT Error Response : ')
        console.log(e)

        return res.json(unauthorizedResponse)
      }
    }

    return res.json(unauthorizedResponse)
  }
}
