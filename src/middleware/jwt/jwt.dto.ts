class UnauthorizedResponse {
  error: boolean = true
  message: string = 'unauthorized'
  status: number = 200
}

export { UnauthorizedResponse }
