import { IsString, IsEmail, IsOptional } from 'class-validator'

class AuthData {
  @IsString({ message: 'Email tidak boleh kosong' })
  @IsEmail({ message: 'Email tidak valid' })
  email: string
}

class AuthLoginData extends AuthData {
  cart_id: string
  fingerprint: string
  fromPayment: boolean
  minicart_id: string

  @IsString({ message: 'Password tidak boleh kosong' })
  password: string

  @IsString({ message: 'Token tidak boleh kosong' })
  token: string

  company_code: string
}

class AuthCheckQueryData extends AuthData {
  @IsString({ message: 'Nomor telepon tidak boleh kosong' })
  phone: string

  @IsString({ message: 'Alamat email tidak boleh kosong' })
  member: any
}

class AuthSocialLoginData {}

class AuthChangePasswordData {
  @IsString({ message: 'Customer ID tidak boleh kosong' })
  customer_id: string
}

class AuthLoginStoreModeData {
  @IsOptional()
  member_number: string | number
}

export {
  AuthLoginData,
  AuthCheckQueryData,
  AuthSocialLoginData,
  AuthChangePasswordData,
  AuthLoginStoreModeData
}
