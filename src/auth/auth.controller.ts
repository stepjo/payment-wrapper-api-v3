import { Controller, Post, Body, HttpCode } from '@nestjs/common'
import { AuthService } from './auth.service'
import {
  AuthLoginData,
  AuthSocialLoginData,
  AuthChangePasswordData,
  AuthLoginStoreModeData
} from './auth.dto'

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('login')
  @HttpCode(200)
  login(@Body() data: AuthLoginData): Promise<any> | object {
    return this.authService.login(data)
  }

  // @Get('check')
  // check(@Query() query: AuthCheckQueryData): Promise<any> | object {
  //   return this.authService.check(query)
  // }

  @Post('authorize-social')
  @HttpCode(200)
  authorizeSocialLogin(
    @Body() data: AuthSocialLoginData
  ): Promise<any> | object {
    return this.authService.authorizeSocialLogin(data)
  }

  @Post('change-password')
  @HttpCode(200)
  changePassword(@Body() data: AuthChangePasswordData): Promise<any> | object {
    return this.authService.changePassword(data)
  }

  @Post('storemode/login')
  @HttpCode(200)
  loginStoreMode(@Body() data: AuthLoginStoreModeData): Promise<any> | object {
    return this.authService.loginStoreMode(data)
  }
}
