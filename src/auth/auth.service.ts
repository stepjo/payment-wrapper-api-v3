import { Injectable, HttpException } from '@nestjs/common'
import { HttpService } from '@nestjs/axios'
import { JwtService } from '@nestjs/jwt'
import { map, catchError } from 'rxjs'
import { defaultHeader, sosmedHeader } from 'src/config/header.config'
import {
  CatchErrorMessage,
  ResponseErrorMessage
} from 'src/utils/error/ErrorMessage.utils'
import { CheckErrorResponse } from 'src/utils/error/ErrorResponse.utils'
import {
  memberConstraints,
  loginStoreModeConstraints
} from 'src/utils/validation/ValidateConstraint.utils'
import { isEmpty, validate } from 'validate.js'
import CryptoJS from 'crypto-js'
import {
  AuthLoginData,
  AuthSocialLoginData,
  AuthLoginStoreModeData,
  AuthChangePasswordData
} from './auth.dto'

@Injectable({})
export class AuthService {
  constructor(
    private httpService: HttpService,
    private jwtService: JwtService
  ) {}

  login(data: AuthLoginData): Promise<any> | object {
    data.company_code = process.env.COMPANY_CODE

    return this.httpService
      .post(
        `${process.env.API_URL}customers/login`,
        { data },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map(async (response: any): Promise<any> => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, '', false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          if (response?.status === 200) {
            const customerId: any = response?.data?.data.customer_id
            const token: string =
              customerId && (await this.jwtService.signAsync(customerId))

            if (token) {
              response.data.data.customer_id = token
            }

            return response?.data
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  // check(query: AuthCheckQueryData): Promise<any> | object {
  //   const email: string = query?.email
  //   const phone: string = query?.phone?.replace('+62', '0')
  //   const member: string = query?.member?.toUpperCase() || null
  //   let header: any = emailHeader(`${email},${phone}`, process.env.COMPANY_CODE)

  //   if (!email || !phone) {
  //     return {
  //       data: {
  //         message: 'bad_parameters'
  //       },
  //       error: false,
  //       message:
  //         'Maaf, alamat email atau nomor telepon Anda tidak boleh kosong.'
  //     }
  //   }

  //   if (member) {
  //     const error: any = validate({ member }, memberConstraints, {
  //       format: 'custom'
  //     })

  //     if (!isEmpty(error)) {
  //       const errorMessage: object = GetErrorMessage({}, null, error[0], 404)

  //       throw new HttpException(errorMessage, 404)
  //     }

  //     header = {
  //       ...header,
  //       'rr-member-number': member
  //     }
  //   }

  //   return this.httpService
  //     .get(`${process.env.API_URL}customers`, { headers: header })
  //     .pipe(
  //       map((response: any): any => {
  //         if (CheckErrorResponse(response?.data?.errors)) {
  //           if (response?.data?.errors?.code === 404) {
  //             return {
  //               data: [],
  //               error: false,
  //               message: 'Alamat email dapat digunakan'
  //             }
  //           } else {
  //             const errorMessage: object = GetErrorMessage(
  //               {},
  //               null,
  //               response?.data.errors?.messages[0] || 'failed',
  //               response?.status || 200
  //             )

  //             throw new HttpException(errorMessage, response?.status || 200)
  //           }
  //         }

  //         if (response?.status === 200) {
  //           const { data }: any = response?.data
  //           const isEmailExist: boolean = get(data, '[0].email', '') === email
  //           const isPhoneExist: boolean = get(data, '[0].phone', '') === phone
  //           const errExistedData: string =
  //             isPhoneExist && isEmailExist
  //               ? 'alamat email dan nomor telepon'
  //               : isPhoneExist
  //               ? 'nomor telepon'
  //               : 'alamat email'

  //           return {
  //             data: {
  //               message: 'customer_exist'
  //             },
  //             error: false,
  //             message: `Maaf, ${errExistedData} Anda telah digunakan. Silahkan gunakan ${errExistedData} lain`
  //           }
  //         }
  //       }),
  //       catchError((e: any): Promise<any> => {
  //         const errorMessage: object = GetErrorMessage(
  //           e?.response?.data?.data || {},
  //           e?.response?.data?.errors || null,
  //           e?.response?.data?.messages[0] || 'failed',
  //           e?.response?.status || 200
  //         )

  //         throw new HttpException(errorMessage, e?.response?.status || 200)
  //       })
  //     )
  // }

  authorizeSocialLogin(data: AuthSocialLoginData): Promise<any> | object {
    const { code, social, fingerprint }: any = data

    return this.httpService
      .get(`${process.env.API_URL}sosmed/authorize_login`, {
        headers: sosmedHeader(code, social, fingerprint)
      })
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const errorMessage: any = ResponseErrorMessage(response, '', true)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          }

          return response?.data
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = ResponseErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  changePassword(data: AuthChangePasswordData): Promise<any> | object {
    const { password, changeForm, customerId }: any = data

    if (!isEmpty(customerId)) {
      const errorMessage: any = ResponseErrorMessage(
        {},
        200,
        false,
        'Customer ID tidak boleh kosong'
      )

      throw new HttpException(errorMessage.payload, errorMessage.status)
    } else {
      try {
        const decipher: any = CryptoJS.AES.encrypt(
          'aes192',
          process.env.JWT_SECRET_KEY
        )

        let decrypted: any = decipher.update(customerId, 'hex', 'utf8')
        decrypted += decipher.final('utf8')

        return this.httpService
          .put(
            `${process.env.API_URL}customers/${decrypted}`,
            {
              data: {
                password,
                change_form:
                  !changeForm || typeof changeForm === 'undefined'
                    ? 0
                    : changeForm
              }
            },
            {
              headers: defaultHeader
            }
          )
          .pipe(
            map((response) => {
              if (CheckErrorResponse(response?.data?.errors)) {
                const errorMessage: any = ResponseErrorMessage(
                  response,
                  200,
                  true
                )

                throw new HttpException(
                  errorMessage.payload,
                  errorMessage.status
                )
              }

              return {
                error: false,
                data: response?.data?.data
              }
            }),
            catchError((e: any): Promise<any> => {
              const errorMessage: any = CatchErrorMessage(e, '', false)

              throw new HttpException(errorMessage.payload, errorMessage.status)
            })
          )
      } catch (e: any) {
        const errorMessage: any = ResponseErrorMessage(
          {},
          200,
          false,
          'Customer Not Valid'
        )

        throw new HttpException(errorMessage.payload, errorMessage.status)
      }
    }
  }

  loginStoreMode(data: AuthLoginStoreModeData): Promise<any> | object {
    if (isEmpty(data)) {
      const errorMessage: any = ResponseErrorMessage(
        {},
        200,
        false,
        'Data tidak lengkap'
      )

      throw new HttpException(errorMessage.payload, errorMessage.status)
    }

    const errorMessages: Array<any> = []
    const authErrors: any = validate(data, loginStoreModeConstraints)

    if (!isEmpty(authErrors)) {
      errorMessages.push(authErrors[0])
    }

    if (!isEmpty(data?.member_number)) {
      const memberErrors: any = validate(
        { member: data?.member_number },
        memberConstraints
      )

      if (!isEmpty(memberErrors)) {
        errorMessages.push(memberErrors[0])
      }
    }

    if (!isEmpty(errorMessages)) {
      const errorMessage: any = ResponseErrorMessage(
        {},
        200,
        false,
        errorMessages[0]
      )

      throw new HttpException(errorMessage.payload, errorMessage.status)
    }

    return this.httpService
      .post(
        `${process.env.API_URL}customers/storemode/login`,
        { data },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            throw response?.data?.errors?.messages
          }

          const { data }: any = response?.data
          const token: any = this.jwtService.sign({
            customerId: data?.customer_id
          })

          return {
            error: false,
            data: {
              data,
              token
            },
            message: 'success'
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }
}
