const phoneNumberPattern = /(^(08))\d{8,12}$/
const memberPattern = /^(AR|ARE|TAM|IR|SC)[0-9]+/
const companyCodePattern = /^(ODI|HCI|AHI)/
const onlyAlphabet = /^[a-zA-Z ]*$/

const loginConstraints: object = {
  email: {
    email: true,
    presence: true
  }
}

const emailOnlyConstraints: object = {
  email: {
    email: true,
    presence: true
  }
}

const loginStoreModeConstraints: object = {
  phone: {
    presence: {
      allowEmpty: false,
      message: '^Nomor telepon tidak boleh kosong'
    },
    format: {
      pattern: phoneNumberPattern,
      message: '^Nomor telepon tidak valid'
    }
  },
  company_code: {
    presence: {
      allowEmpty: false,
      message: '^Jenis Account tidak boleh kosong'
    },
    format: {
      pattern: companyCodePattern,
      message: '^Jenis Account tidak valid'
    }
  }
}

const memberConstraints = {
  member: {
    presence: {
      allowEmpty: false,
      message: '^Member tidak boleh kosong'
    },
    format: {
      pattern: memberPattern,
      message: '^Member Anda tidak valid'
    }
  }
}

const addressConstraints = {
  address_name: {
    presence: { message: '^Nama alamat tidak boleh kosong' }
  },
  first_name: {
    presence: { allowEmpty: false, message: '^Nama depan tidak boleh kosong' },
    format: {
      pattern: onlyAlphabet,
      message: '^Nama depan tidak valid'
    }
  },
  phone: {
    numericality: { message: '^Nomor telepon harus angka' }
  },
  full_address: {
    presence: { message: '^Alamat tidak boleh kosong' }
  },
  post_code: {
    numericality: { message: '^Kode pos harus angka' }
  },
  'province.province_id': {
    presence: true,
    numericality: true
  },
  'city.city_id': {
    presence: true,
    numericality: true
  },
  'kecamatan.kecamatan_id': {
    presence: true,
    numericality: true
  }
}

export {
  loginConstraints,
  emailOnlyConstraints,
  loginStoreModeConstraints,
  memberConstraints,
  addressConstraints
}
