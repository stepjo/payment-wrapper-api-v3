const CheckErrorResponse = (data: any): string | boolean => {
  if (Array.isArray(data)) {
    return data.length > 0
  }

  return data?.code || data?.title
}

const GetErrorResponse = (data: any): Error => {
  if (data.hasOwnProperty('message')) {
    return new Error(data?.message)
  }

  return new Error(data?.message[0]?.replace('Error: ', ''))
}

export { CheckErrorResponse, GetErrorResponse }
