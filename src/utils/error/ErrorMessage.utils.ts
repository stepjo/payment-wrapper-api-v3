const ResponseErrorMessage = (
  response: any,
  statusCode: string | number,
  fromMessagesAttr: boolean,
  customMessage: string = '',
  customError: object | null = null,
  additional: object | null = null
): object => {
  const status =
    statusCode || response?.data?.errors?.code || response?.status || 200

  let payload: object = {
    data: response?.data?.data || {},
    errors: customError || response?.data?.errors || {},
    error: true,
    message:
      customMessage || fromMessagesAttr
        ? response?.data?.errors?.messages || 'failed'
        : response?.data?.errors?.messages[0] ||
          response?.data?.errors?.title ||
          response?.problem ||
          'failed',
    status
  }

  if (additional) {
    payload = { ...payload, ...additional }
  }

  return { payload, status }
}

const CatchErrorMessage = (
  e: any,
  statusCode: string | number,
  fromMessagesAttr: boolean,
  customMessage: string = '',
  customError: object | string | null = null,
  additional: object | null = null
): object => {
  const status: string | number =
    statusCode || e?.response?.data?.errors?.code || e?.response?.status || 200

  let message: any = 'failed'

  if (customMessage) {
    message = customMessage
  } else if (e?.response?.message) {
    message = e?.response?.message
  } else if (e?.response?.errors && typeof e?.response?.errors === 'string') {
    message = e?.response?.errors
  } else if (
    e?.response?.data?.errors &&
    typeof e?.response?.data?.errors === 'string'
  ) {
    message = e?.response?.data?.errors
  } else if (fromMessagesAttr && e?.response?.data?.errors?.messages) {
    message = e?.response?.data?.errors?.messages
  } else if (e.response?.data?.errors?.messages?.[0]) {
    message = e.response?.data?.errors?.messages?.[0]
  }

  let payload: object = {
    data: e?.response?.data?.data || e?.response?.data || {},
    errors:
      customError || e?.response?.errors || e?.response?.data?.errors || {},
    error: true,
    message,
    status
  }

  if (additional) {
    payload = { ...payload, ...additional }
  }

  return { payload, status }
}

export { CatchErrorMessage, ResponseErrorMessage }
