// BASE
import { Injectable, HttpException } from '@nestjs/common'
import { HttpService } from '@nestjs/axios'
import { map, catchError } from 'rxjs'

// CONFIG
import { cartHeader } from 'src/config/header.config'

// UTILS
import { CatchErrorMessage } from 'src/utils/error/ErrorMessage.utils'

// DTO
import { GetVoucherResponse } from './voucher.dto'

@Injectable({})
export class VoucherService {
  constructor(private httpService: HttpService) {}

  getVoucher(id: string) {
    return this.httpService
      .get(`${process.env.CART_API_URL}marketing/voucher/wallet/${id}`, {
        headers: cartHeader
      })
      .pipe(
        map((response) => {
          const getVoucherResponse = new GetVoucherResponse(
            false,
            'success',
            response?.data?.data || {}
          )

          // SUCCESS
          if (response?.status === 200) {
            // NO DATA
            if (!response.data) {
              getVoucherResponse.error = true
              getVoucherResponse.message = 'Something went wrong'

              return getVoucherResponse
            }

            return getVoucherResponse
          }

          getVoucherResponse.error = true
          getVoucherResponse.message =
            response?.data?.errors || 'Something went wrong, response not ok'

          return GetVoucherResponse
        }),
        catchError((e) => {
          const errorMessage: any = CatchErrorMessage(e, '', false, '', {
            error: true
          })

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }
}
