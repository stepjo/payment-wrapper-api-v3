// BASE
import { Module } from '@nestjs/common'
import { HttpModule } from '@nestjs/axios'

// CONTROLLER
import { VoucherController } from './voucher.controller'

// SERVICE
import { VoucherService } from './voucher.service'

@Module({
  imports: [
    HttpModule.register({
      timeout: 60000
    })
  ],
  controllers: [VoucherController],
  providers: [VoucherService]
})
export class VoucherModule {}
