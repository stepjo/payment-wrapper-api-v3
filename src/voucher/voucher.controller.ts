// BASE
import { Controller, Get, Param } from '@nestjs/common'

// SERVICES
import { VoucherService } from './voucher.service'

@Controller('voucher')
export class VoucherController {
  constructor(private voucherService: VoucherService) {}

  @Get(':id')
  getVoucher(@Param('id') id: string) {
    return this.voucherService.getVoucher(id)
  }
}
