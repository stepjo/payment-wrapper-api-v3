// RESPONSE
class GetVoucherResponse {
  constructor(error: boolean, message: string, data: object) {
    this.error = error
    this.message = message
    this.data = data
  }

  data: object
  error: boolean
  message: string
}

export { GetVoucherResponse }
