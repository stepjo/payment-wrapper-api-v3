import { Module } from '@nestjs/common'
import { AuthModule } from './auth/auth.module'
import { CartModule } from './cart/cart.module'
import { MiscModule } from './misc/misc.module'
import { PaymentModule } from './payment/payment.module'
import { VoucherModule } from './voucher/voucher.module'
import { UserModule } from './user/user.module'
import { GosendModule } from './gosend/gosend.module'
import { ConfigModule } from '@nestjs/config'

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env', '.env.development'],
      isGlobal: true
    }),
    AuthModule,
    CartModule,
    MiscModule,
    PaymentModule,
    VoucherModule,
    UserModule,
    GosendModule
  ]
})
export class AppModule {}
