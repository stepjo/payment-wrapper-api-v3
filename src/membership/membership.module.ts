import { MiddlewareConsumer, Module } from '@nestjs/common'
import { HttpModule } from '@nestjs/axios'
import { JwtModule } from '@nestjs/jwt'
import { JwtVerifyAddressMiddleware } from 'src/middleware/jwt/JwtVerifyAddress.middleware'
import { MembershipController } from './membership.controller'
import { MembershipService } from './membership.service'

@Module({
  imports: [
    HttpModule.register({
      timeout: 60000
    }),
    ,
    JwtModule.registerAsync({
      useFactory: () => ({
        secret: process.env.JWT_SECRET_KEY
      }),
      imports: undefined
    })
  ],
  controllers: [MembershipController],
  providers: [MembershipService]
})
export class CartModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(JwtVerifyAddressMiddleware).forRoutes('ace/pay-with-point')
  }
}
