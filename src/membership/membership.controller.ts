import { Controller, Post, Body, HttpCode, Query } from '@nestjs/common'
import { MembershipService } from './membership.service'
import {
  MembershipPaymentPointData,
  MembershipPaymentPointQuery
} from './membership.dto'

@Controller('membership')
export class MembershipController {
  constructor(private membershipService: MembershipService) {}

  @Post('ace/pay-with-point')
  @HttpCode(200)
  paymentPoint(
    @Query() query: MembershipPaymentPointQuery,
    @Body() data: MembershipPaymentPointData
  ): any {
    return this.membershipService.paymentPoint(query, data)
  }
}
