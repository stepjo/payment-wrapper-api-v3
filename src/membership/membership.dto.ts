class MembershipPaymentPointQuery {
  customer_id: string
}
class MembershipPaymentPointData {
  otp_number: string | number
  point_redeem: string | number
  cart_id: string
}

export { MembershipPaymentPointQuery, MembershipPaymentPointData }
