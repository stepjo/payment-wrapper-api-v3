import { Injectable, HttpException } from '@nestjs/common'
import { HttpService } from '@nestjs/axios'
import { map, catchError } from 'rxjs'
import { tpsHeader } from 'src/config/header.config'
import { CatchErrorMessage } from 'src/utils/error/ErrorMessage.utils'
import {
  MembershipPaymentPointData,
  MembershipPaymentPointQuery
} from './membership.dto'
import { isEmpty } from 'validate.js'

@Injectable({})
export class MembershipService {
  constructor(private httpService: HttpService) {}
  paymentPoint(
    query: MembershipPaymentPointQuery,
    data: MembershipPaymentPointData
  ) {
    const body: object = {
      customer_id: query?.customer_id || '',
      otp_number: data?.otp_number || '',
      point_redeem: data?.point_redeem || '',
      cart_id: data?.cart_id || ''
    }

    return this.httpService
      .post(
        `${process.env.API_URL}ace/pwp/redeem`,
        { data: body },
        {
          headers: tpsHeader
        }
      )
      .pipe(
        map((response: any): Promise<any> | object => {
          if (response?.status === 200) {
            if (!isEmpty(response?.data?.errors)) {
              return {
                error: true,
                data: {},
                message:
                  response?.data?.errors?.messages || 'Data tidak ditemukan'
              }
            } else {
              return {
                error: false,
                data: response?.data?.data,
                message: response?.data?.message
              }
            }
          } else {
            return {
              error: true,
              data: {},
              message: 'failed'
            }
          }
        }),
        catchError((e: any): any => {
          if (e?.response?.status === 400 && e?.response?.data?.errors) {
            return {
              error: true,
              data: {},
              message: e?.response?.data?.errors?.message
            }
          }

          const errorMessage: any = CatchErrorMessage(
            e,
            '',
            false,
            '',
            null,
            null
          )

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }
}
