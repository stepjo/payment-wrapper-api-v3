import { Module } from '@nestjs/common'
import { HttpModule } from '@nestjs/axios'
import { MoappController } from './moapp.controller'
import { MoappService } from './moapp.service'

@Module({
  imports: [
    HttpModule.register({
      timeout: 60000
    })
  ],
  controllers: [MoappController],
  providers: [MoappService]
})
export class MoappModule {}
