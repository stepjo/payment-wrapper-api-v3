import { Controller, Post, Body, HttpCode } from '@nestjs/common'
import {
  MoappRatingCheckData,
  MoappRatingSaveData,
  MoappRatingCheckAceData,
  MoappRatingSaveAceData,
  MoappRatingCheckInformaData,
  MoappRatingSaveInformaData
} from './moapp.dto'
import { MoappService } from './moapp.service'

@Controller('moapp')
export class MoappController {
  constructor(private moappService: MoappService) {}

  @Post('rating/check')
  @HttpCode(200)
  ratingCheck(@Body() data: MoappRatingCheckData): Promise<any> | object {
    return this.moappService.ratingCheck(data)
  }

  @Post('rating')
  @HttpCode(200)
  ratingSave(@Body() data: MoappRatingSaveData): Promise<any> | object {
    return this.moappService.ratingSave(data)
  }

  @Post('ace/rating/check')
  @HttpCode(200)
  ratingCheckAce(@Body() data: MoappRatingCheckAceData): Promise<any> | object {
    return this.moappService.ratingCheckAce(data)
  }

  @Post('ace/rating')
  @HttpCode(200)
  ratingSaveAce(@Body() data: MoappRatingSaveAceData): Promise<any> | object {
    return this.moappService.ratingSaveAce(data)
  }

  @Post('informa/rating/check')
  @HttpCode(200)
  ratingCheckInforma(
    @Body() data: MoappRatingCheckInformaData
  ): Promise<any> | object {
    return this.moappService.ratingCheckInforma(data)
  }

  @Post('informa/rating')
  @HttpCode(200)
  ratingSaveInforma(
    @Body() data: MoappRatingSaveInformaData
  ): Promise<any> | object {
    return this.moappService.ratingSaveInforma(data)
  }
}
