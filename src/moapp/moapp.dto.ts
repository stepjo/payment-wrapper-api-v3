class MoappRatingCheckData {}
class MoappRatingSaveData {}
class MoappRatingCheckAceData {}
class MoappRatingSaveAceData {}
class MoappRatingCheckInformaData {}
class MoappRatingSaveInformaData {}
export {
  MoappRatingCheckData,
  MoappRatingSaveData,
  MoappRatingCheckAceData,
  MoappRatingSaveAceData,
  MoappRatingCheckInformaData,
  MoappRatingSaveInformaData
}
