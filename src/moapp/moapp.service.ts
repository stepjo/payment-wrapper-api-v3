import { Injectable, HttpException } from '@nestjs/common'
import { HttpService } from '@nestjs/axios'
import { map, catchError } from 'rxjs'
import { defaultHeader } from 'src/config/header.config'
import { CatchErrorMessage } from 'src/utils/error/ErrorMessage.utils'
import { CheckErrorResponse } from 'src/utils/error/ErrorResponse.utils'
import {
  MoappRatingCheckData,
  MoappRatingCheckInformaData,
  MoappRatingSaveAceData,
  MoappRatingSaveData,
  MoappRatingSaveInformaData
} from './moapp.dto'
@Injectable({})
export class MoappService {
  constructor(private httpService: HttpService) {}

  ratingCheck(data: MoappRatingCheckData): Promise<any> | object {
    return this.httpService
      .post(
        `${process.env.CART_API_URL}moapp/rating/check`,
        { data },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response: any): any => {
          if (CheckErrorResponse(response?.data?.errors)) {
            return {
              errors: response?.data?.errors,
              message: 'failed',
              data: response?.data?.data
            }
          }

          return {
            errors: {},
            message: 'success',
            data: response?.data?.data
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  ratingSave(data: MoappRatingSaveData): Promise<any> | object {
    return this.httpService
      .post(
        `${process.env.CART_API_URL}moapp/rating`,
        { data },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response: any): Promise<any> | object => {
          if (CheckErrorResponse(response?.data?.errors)) {
            return {
              errors: response?.data?.errors,
              message: 'failed',
              data: response?.data?.data
            }
          }
          return {
            errors: {},
            message: 'success',
            data: response?.data?.data
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  ratingCheckAce(data: any) {
    return this.httpService
      .post(
        `${process.env.CART_API_URL}moapp/ace/rating/check`,
        { data },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response: any): any => {
          if (CheckErrorResponse(response?.data?.errors)) {
            return {
              errors: response?.data?.errors,
              message: 'failed',
              data: response?.data?.data
            }
          }

          return {
            errors: {},
            message: 'success',
            data: response?.data?.data
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  ratingSaveAce(data: MoappRatingSaveAceData): Promise<any> | object {
    return this.httpService
      .post(
        `${process.env.CART_API_URL}moapp/ace/rating`,
        { data },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response: any): Promise<any> | object => {
          if (CheckErrorResponse(response?.data?.errors)) {
            return {
              errors: response?.data?.errors,
              message: 'failed',
              data: response?.data?.data
            }
          }

          return {
            errors: {},
            message: 'success',
            data: response?.data?.data
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  ratingCheckInforma(data: MoappRatingCheckInformaData) {
    return this.httpService
      .post(
        `${process.env.CART_API_URL}moapp/informa/rating/check`,
        { data },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response: any): any => {
          if (CheckErrorResponse(response?.data?.errors)) {
            return {
              errors: response?.data?.errors,
              message: 'failed',
              data: response?.data?.data
            }
          }

          return {
            errors: {},
            message: 'success',
            data: response?.data?.data
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  ratingSaveInforma(data: MoappRatingSaveInformaData): Promise<any> | object {
    return this.httpService
      .post(
        `${process.env.CART_API_URL}moapp/informa/rating`,
        { data },
        {
          headers: defaultHeader
        }
      )
      .pipe(
        map((response: any): Promise<any> | object => {
          if (CheckErrorResponse(response?.data?.errors)) {
            return {
              errors: response?.data?.errors,
              message: 'failed',
              data: response?.data?.data
            }
          }

          return {
            errors: {},
            message: 'success',
            data: response?.data?.data
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }
}
