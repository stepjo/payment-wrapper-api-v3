import { Controller, Post, Body, HttpCode } from '@nestjs/common'
import { BlueBirdRedeemVoucherData } from './bluebird.dto'
import { BlueBirdService } from './bluebird.service'

@Controller('bluebird')
export class BlueBirdController {
  constructor(private blueBirdService: BlueBirdService) {}

  @Post('calculate')
  @HttpCode(200)
  redeemVoucher(@Body() data: BlueBirdRedeemVoucherData) {
    return this.blueBirdService.calculate(data)
  }
}
