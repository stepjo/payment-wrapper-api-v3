import { Injectable, HttpException } from '@nestjs/common'
import { HttpService } from '@nestjs/axios'
import { map, catchError } from 'rxjs'
import { defaultHeader } from 'src/config/header.config'
import { CatchErrorMessage } from 'src/utils/error/ErrorMessage.utils'
import { BlueBirdRedeemVoucherData } from './bluebird.dto'
import { isEmpty } from 'validate.js'

const expressCourierValidate = async (
  httpService: HttpService,
  destinationGeolocation: string | number,
  sku: string | number,
  carrierId: string | number,
  customerAddressId: any,
  cartId: string,
  price: string | number
): Promise<any> => {
  return await httpService
    .get(
      `${process.env.CART_API_URL}cart/assignation/origin?cart_id=${cartId}&sku=${sku}&customer_address_id=${customerAddressId}&carrier_id=${carrierId}&destination_geolocation=${destinationGeolocation}`
    )
    .pipe(
      map(async (response: any): Promise<any> => {
        if (response?.status === 200) {
          const checkGeolocationData: any = response?.data

          if (!isEmpty(checkGeolocationData?.errors)) {
            return {
              error: false,
              message: checkGeolocationData?.errors?.message,
              data: {}
            }
          } else {
            const geolocationData: any = checkGeolocationData?.data

            const data: object = {
              origin: geolocationData?.geolocation,
              destination: destinationGeolocation,
              price: price
            }

            await httpService
              .post(
                `${process.env.SHIPMENT_API_URL}bluebird/calculate`,
                { data },
                { headers: defaultHeader }
              )
              .pipe(
                map(async (response: any): Promise<any> => {
                  if (response?.status === 200) {
                    const responseData: any = response?.data?.data

                    if (!isEmpty(responseData)) {
                      return {
                        error: false,
                        message: 'Validate Success',
                        data: responseData
                      }
                    } else {
                      return {
                        error: false,
                        message: 'Message Error',
                        data: {}
                      }
                    }
                  } else {
                    return {
                      error: true,
                      message: response?.problem,
                      data: {}
                    }
                  }
                }),
                catchError((e: any): Promise<any> => {
                  const errorMessage: any = CatchErrorMessage(e, '', false)

                  throw new HttpException(
                    errorMessage.payload,
                    errorMessage.status
                  )
                })
              )
          }
        } else {
          const checkGeolocationData: any = response?.data

          if (!isEmpty(checkGeolocationData?.errors)) {
            return {
              error: true,
              message: checkGeolocationData?.errors?.message,
              data: {}
            }
          }
        }
      }),
      catchError((e: any): Promise<any> => {
        const errorMessage: any = CatchErrorMessage(e, '', false)

        throw new HttpException(errorMessage.payload, errorMessage.status)
      })
    )
}

@Injectable({})
export class BlueBirdService {
  constructor(private httpService: HttpService) {}
  calculate(body: BlueBirdRedeemVoucherData): Promise<any> | object {
    const addressId: string | number = body?.address_id
    const sku: string | number = body?.sku
    const cartId: string = body?.cart_id
    const destinationGeolocation: string | number =
      body?.destination_geolocation
    const carrierId: string | number = body?.carrier_id
    const price: string | number = body?.price

    if (addressId) {
      const data: object = expressCourierValidate(
        this.httpService,
        destinationGeolocation,
        sku,
        carrierId,
        addressId,
        cartId,
        price
      )

      return data
    }

    return {}
  }
}
