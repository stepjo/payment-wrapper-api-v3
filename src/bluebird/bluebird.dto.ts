class BlueBirdRedeemVoucherData {
  address_id: string
  sku: string | number
  cart_id: string
  destination_geolocation: string
  carrier_id: string | number
  price: number
}

export { BlueBirdRedeemVoucherData }
