import { Module } from '@nestjs/common'
import { HttpModule } from '@nestjs/axios'
import { BlueBirdController } from './bluebird.controller'
import { BlueBirdService } from './bluebird.service'

@Module({
  imports: [
    HttpModule.register({
      timeout: 60000
    })
  ],
  controllers: [BlueBirdController],
  providers: [BlueBirdService]
})
export class CartModule {}
