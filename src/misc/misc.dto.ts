class MiscPickupLocationData {
  cart_id: string
}

class MiscCmsBlockQuery {
  identifier: string
}

class MiscTokenLogData {}

export { MiscPickupLocationData, MiscCmsBlockQuery, MiscTokenLogData }
