import { MiddlewareConsumer, Module } from '@nestjs/common'
import { HttpModule } from '@nestjs/axios'
import { JwtModule } from '@nestjs/jwt'
import { MiscController } from './misc.controller'
import { MiscService } from './misc.service'
import { JwtVerifyAddressMiddleware } from 'src/middleware/jwt/JwtVerifyAddress.middleware'
@Module({
  imports: [
    HttpModule,
    JwtModule.registerAsync({
      useFactory: () => ({
        secret: process.env.JWT_SECRET_KEY
      }),
      imports: undefined
    })
  ],
  controllers: [MiscController],
  providers: [MiscService]
})
export class MiscModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(JwtVerifyAddressMiddleware).forRoutes('misc/faktur-pajak')
  }
}
