import { Injectable, HttpException } from '@nestjs/common'
import { HttpService } from '@nestjs/axios'
import { map, catchError } from 'rxjs'
import { defaultHeader, sosmedHeader } from 'src/config/header.config'
import {
  ResponseErrorMessage,
  CatchErrorMessage
} from 'src/utils/error/ErrorMessage.utils'
import {
  MiscCmsBlockQuery,
  MiscPickupLocationData,
  MiscTokenLogData
} from './misc.dto'
import { isEmpty } from 'validate.js'

@Injectable({})
export class MiscService {
  constructor(private httpService: HttpService) {}

  getProvince(): Promise<any> | object {
    return this.httpService
      .get(`${process.env.API_URL}master/country/102`, {
        headers: defaultHeader
      })
      .pipe(
        map((response) => {
          return {
            error: false,
            data: response?.data?.data?.province
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  getCity(param: any): Promise<any> | object {
    return this.httpService
      .get(`${process.env.API_URL}master/province/${param?.id}`, {
        headers: defaultHeader
      })
      .pipe(
        map((response) => {
          return {
            error: false,
            data: response?.data?.data?.city
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  getKecamatan(param: any) {
    return this.httpService
      .get(`${process.env.API_URL}master/city/${param?.id}`, {
        headers: defaultHeader
      })
      .pipe(
        map((response) => {
          return {
            error: false,
            data: response?.data?.data?.kecamatan
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  getCmsBlockDetail(query: MiscCmsBlockQuery): Promise<any> | object {
    if (typeof query?.identifier !== 'undefined') {
      return this.httpService
        .get(
          `cms_block/detail?cms_block=${query.identifier}&company_code=${process.env.COMPANY_CODE}`,
          {
            headers: defaultHeader
          }
        )
        .pipe(
          map((response) => {
            if (response?.status === 200 && response?.data?.data?.status > 0) {
              return {
                error: false,
                data: response?.data?.data
              }
            }

            return {
              error: 'not found',
              data: {}
            }
          }),
          catchError((e: any): Promise<any> => {
            const errorMessage: any = CatchErrorMessage(e, '', false)

            throw new HttpException(errorMessage.payload, errorMessage.status)
          })
        )
    } else {
      const errorMessage: any = ResponseErrorMessage({}, '', false, 'Not found')

      throw new HttpException(errorMessage.payload, errorMessage.status)
    }
  }

  getFakturPajak(query: any): Promise<any> | object {
    return this.httpService
      .get(`${process.env.API_URL}customer/company/${query.customerId}`, {
        headers: defaultHeader
      })
      .pipe(
        map((response: any): Promise<any> | object => {
          if (response?.status === 200) {
            if (!isEmpty(response?.data?.data)) {
              return {
                error: false,
                data: response?.data?.data,
                message: 'success'
              }
            } else {
              return {
                error: 'not found',
                data: {}
              }
            }
          } else {
            return {
              error: 'not found',
              data: {}
            }
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  getUrlSocialLogin() {
    return this.httpService
      .get(`${process.env.API_URL}sosmed/login_url`, {
        headers: sosmedHeader('facebook', '', '')
      })
      .pipe(
        map((response: any): any => {
          if (response?.status === 200) {
            const facebookURL: any = response?.data?.data

            if (facebookURL) {
              this.httpService
                .get(`${process.env.API_URL}sosmed/login_url`, {
                  headers: sosmedHeader('google', '', '')
                })
                .pipe(
                  map((response: any): any => {
                    if (response?.status === 200) {
                      const googleURL: any = response?.data?.data

                      if (googleURL) {
                        return {
                          error: false,
                          data: {
                            facebookURL,
                            googleURL
                          }
                        }
                      }
                    }
                  }),
                  catchError((e: any): Promise<any> => {
                    const errorMessage: any = CatchErrorMessage(e, '', false)

                    throw new HttpException(
                      errorMessage.payload,
                      errorMessage.status
                    )
                  })
                )
            } else {
            }
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  //@todo: change code
  // async addFakturPajak() {
  //   return this.httpService
  //     .post(`${process.env.CART_API_URL}marketing/voucher/redeem`, payload, {
  //       headers: defaultHeader
  //     })
  //     .pipe(
  //       map((response: any): any => {
  //         if (CheckErrorResponse(response?.data?.errors)) {
  //           return {
  //             errors: response?.data?.errors,
  //             message: 'failed',
  //             data: response?.data?.data
  //           }
  //         }
  //         if (response?.status === 200) {
  //           return {
  //             errors: response?.data?.errors,
  //             message: 'success',
  //             data: response?.data?.data
  //           }
  //         }
  //       }),
  //       catchError((e: any): any => {
  //         const errorMessage: object = GetErrorMessage(
  //           e?.response?.data?.data || {},
  //           e?.response?.data?.errors || null,
  //           'failed'
  //         )

  //         throw new HttpException(errorMessage, e?.response?.status || 200)
  //       })
  //     )
  // }

  getPickupLocation(header: any, data: MiscPickupLocationData) {
    if (!isEmpty(data) && data?.cart_id) {
      return this.httpService
        .post(`${process.env.CART_API_URL}cart/item/pickup_location`, data, {
          headers: defaultHeader
        })
        .pipe(
          map((response) => {
            if (
              header['user-agent'] === 'informa-android' ||
              header['user-agent'] === 'informa-ios' ||
              header['user-agent'] === 'informa-huawei'
            ) {
              response.data.data = response?.data?.data?.filter(
                (element: any) => {
                  const name: string = element?.pickup_name?.toLowerCase()

                  return (
                    !name?.includes('selma') &&
                    !name?.includes('susen') &&
                    !name?.includes('ace')
                  )
                }
              )
            }

            return response?.data
          }),
          catchError((e: any): Promise<any> => {
            const errorMessage: any = CatchErrorMessage(e, 200, false, '', {
              code: 500,
              message: 'Error ocurred when trying to get pickup location list',
              data: {
                type: 'global',
                message: 'Error ocurred when trying to get pickup location list'
              }
            })

            throw new HttpException(errorMessage.payload, errorMessage.status)
          })
        )
    }

    return {}
  }

  tokenLog(data: MiscTokenLogData): Promise<any> | object {
    const body: string = JSON.stringify(data)
    console.log(body)

    return {
      error: false,
      message: 'success'
    }
  }
}
