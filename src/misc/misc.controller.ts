import {
  Controller,
  Get,
  Post,
  Body,
  HttpCode,
  Param,
  Query,
  Headers
} from '@nestjs/common'
import {
  MiscCmsBlockQuery,
  MiscPickupLocationData,
  MiscTokenLogData
} from './misc.dto'
import { MiscService } from './misc.service'

@Controller('misc')
export class MiscController {
  constructor(private miscService: MiscService) {}

  @Get('province')
  getProvince(): Promise<any> | object {
    return this.miscService.getProvince()
  }

  @Get('city/:id')
  getCity(@Param() param: any): Promise<any> | object {
    return this.miscService.getCity(param)
  }

  @Get('kecamatan/:id')
  getKecamatan(@Param() param: any): Promise<any> | object {
    return this.miscService.getKecamatan(param)
  }

  @Get('cms-block-detail')
  getCmsBlockDetail(@Query() query: MiscCmsBlockQuery): Promise<any> | object {
    return this.miscService.getCmsBlockDetail(query)
  }

  @Get('faktur-pajak')
  getFakturPajak(@Query() query: any): Promise<any> | object {
    return this.miscService.getFakturPajak(query)
  }

  @Get('url-social-login')
  getUrlSocialLogin(): Promise<any> | object {
    return this.miscService.getUrlSocialLogin()
  }

  // @todo: check code
  // @Post('add-faktur')
  // @HttpCode(200)
  // addFakturPajak(@Body() data: any): Promise<any> | object {
  //   return this.miscService.addFakturPajak(data)
  // }

  @Post('pickup_location')
  @HttpCode(200)
  getPickupLocation(
    @Headers() header: any,
    @Body() data: MiscPickupLocationData
  ): Promise<any> | object {
    return this.miscService.getPickupLocation(header, data)
  }

  @Post('token-log')
  @HttpCode(200)
  tokenLog(@Body() data: MiscTokenLogData): Promise<any> | object {
    return this.miscService.tokenLog(data)
  }
}
