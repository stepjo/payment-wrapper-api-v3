class GiftCardParam {
  cartId: string
}

class RedeemVoucherParam extends GiftCardParam {}
class DeleteVoucherParam extends GiftCardParam {}
class RedeemVoucherData {}
class DeleteVoucherData {}

export {
  RedeemVoucherData,
  RedeemVoucherParam,
  DeleteVoucherData,
  DeleteVoucherParam
}
