import { Controller, Post, Body, HttpCode, Param } from '@nestjs/common'
import { GiftCardService } from './giftcard.service'
import {
  RedeemVoucherData,
  RedeemVoucherParam,
  DeleteVoucherData,
  DeleteVoucherParam
} from './giftcard.dto'

@Controller('voucher')
export class GiftCardController {
  constructor(private giftCardService: GiftCardService) {}

  @Post('redeem/:cartId')
  @HttpCode(200)
  redeemVoucher(
    @Param() param: RedeemVoucherParam,
    @Body() data: RedeemVoucherData
  ): Promise<any> | object {
    return this.giftCardService.redeemVoucher(param, data)
  }

  @Post('delete/:cartId')
  @HttpCode(200)
  deleteVoucher(
    @Param() param: DeleteVoucherParam,
    @Body() data: DeleteVoucherData
  ): Promise<any> | object {
    return this.giftCardService.deleteVoucher(param, data)
  }
}
