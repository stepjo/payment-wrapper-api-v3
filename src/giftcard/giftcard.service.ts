import { Injectable, HttpException } from '@nestjs/common'
import { HttpService } from '@nestjs/axios'
import { map, catchError } from 'rxjs'
import { defaultHeader } from 'src/config/header.config'
import {
  ResponseErrorMessage,
  CatchErrorMessage
} from 'src/utils/error/ErrorMessage.utils'
import { CheckErrorResponse } from 'src/utils/error/ErrorResponse.utils'
import {
  RedeemVoucherParam,
  RedeemVoucherData,
  DeleteVoucherParam,
  DeleteVoucherData
} from './giftcard.dto'
import { isEmpty } from 'validate.js'

@Injectable({})
export class GiftCardService {
  constructor(private httpService: HttpService) {}

  redeemVoucher(
    param: RedeemVoucherParam,
    data: RedeemVoucherData
  ): Promise<any> | object {
    if (!data || isEmpty(param?.cartId)) {
      const errorMessage: any = ResponseErrorMessage({}, 200, false, 'Error')

      throw new HttpException(errorMessage.payload, errorMessage.status)
    }

    const payload: object = {
      ...data,
      cart_id: param.cartId
    }

    return this.httpService
      .post(`${process.env.CART_API_URL}marketing/voucher/redeem`, payload, {
        headers: defaultHeader
      })
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            const urlRegEx: any = new RegExp(
              '([a-zA-Z0-9]+://)?([a-zA-Z0-9_]+:[a-zA-Z0-9_]+@)?([a-zA-Z0-9.-]+\\.[A-Za-z]{2,4})(:[0-9]+)?([^ ])+'
            )

            let voucherType: any = ''

            if (response?.data?.errors?.message_data[0]?.match(urlRegEx)) {
              voucherType = {
                voucher_type: 'link'
              }
            } else if (
              response?.data?.errors?.message?.includes('pembelanjaan') &&
              response?.data?.errors?.message?.includes('toko')
            ) {
              voucherType = {
                voucher_type: 'store'
              }
            } else if (response?.data?.errors?.message?.includes('kali')) {
              voucherType = {
                voucher_type: 'total'
              }
            } else if (
              response?.data?.errors?.message?.includes('kategori/Brand')
            ) {
              voucherType = {
                voucher_type: 'category'
              }
            } else if (
              response?.data?.errors?.message?.includes('maksimal') &&
              response?.data?.errors?.message?.includes('produk')
            ) {
              voucherType = {
                voucher_type: 'number_product'
              }
            } else if (response?.data?.errors?.message?.includes('buah')) {
              voucherType = {
                voucher_type: 'minimum_link'
              }
            } else if (response?.data?.errors?.message?.includes('mencukupi')) {
              voucherType = {
                voucher_type: 'price'
              }
            } else if (response?.data?.errors?.message?.includes('member')) {
              voucherType = {
                voucher_type: 'member'
              }
            } else if (!response?.data?.errors?.message_data) {
              voucherType = ''
            } else {
              voucherType = {
                voucher_type: 'product'
              }
            }

            return {
              errors: { ...response?.data?.errors, voucherType },
              message: 'failed',
              data: response?.data?.data
            }
          }

          return {
            errors: response?.data?.errors,
            message: 'success',
            data: response?.data?.data
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }

  deleteVoucher(param: DeleteVoucherParam, data: DeleteVoucherData) {
    if (!data || isEmpty(param?.cartId)) {
      const errorMessage: any = ResponseErrorMessage({}, '', false, 'Error')

      throw new HttpException(errorMessage.payload, errorMessage.status)
    }

    const payload: object = {
      ...data,
      cart_id: param?.cartId
    }

    return this.httpService
      .post(`${process.env.CART_API_URL}marketing/voucher/redeem`, payload, {
        headers: defaultHeader
      })
      .pipe(
        map((response) => {
          if (CheckErrorResponse(response?.data?.errors)) {
            return {
              errors: response?.data?.errors,
              message: 'failed',
              data: response?.data?.data
            }
          }

          return {
            errors: response?.data?.errors,
            message: 'success',
            data: response?.data?.data
          }
        }),
        catchError((e: any): Promise<any> => {
          const errorMessage: any = CatchErrorMessage(e, '', false)

          throw new HttpException(errorMessage.payload, errorMessage.status)
        })
      )
  }
}
