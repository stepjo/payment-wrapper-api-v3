import { Module } from '@nestjs/common'
import { HttpModule } from '@nestjs/axios'
import { GiftCardController } from './giftcard.controller'
import { GiftCardService } from './giftcard.service'

@Module({
  imports: [
    HttpModule.register({
      timeout: 60000
    })
  ],
  controllers: [GiftCardController],
  providers: [GiftCardService]
})
export class CartModule {}
