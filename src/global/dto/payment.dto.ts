class Payment {
  save_promotion?: boolean
  method?: string
}

class PaymentVa extends Payment {
  bank_transfer?: string
  va_bank: string
}

export { Payment, PaymentVa }
