class Province {
  province_id: string | number
}

class City {
  city_id: string | number
}

class District {
  kecamatan_id: string | number
}

export { Province, City, District }
