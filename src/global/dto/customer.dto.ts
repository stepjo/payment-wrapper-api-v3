class Customer {
  customer_id: string | number
  customer_is_guest?: number
  customer_email?: string
}

export { Customer }
